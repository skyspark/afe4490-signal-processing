#ifndef CCRC16_TEST_H
#define CCRC16_TEST_H

#include <QObject>

class cCrc16_test : public QObject
{
    Q_OBJECT
public:
    explicit cCrc16_test(QObject *parent = 0);

signals:

public slots:
private slots:
    void test();
};

#endif // CCRC16_TEST_H
