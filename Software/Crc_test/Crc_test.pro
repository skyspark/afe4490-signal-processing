QT += core
QT -= gui

QT += testlib

CONFIG += c++11

TARGET = Crc_test
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

include("../photologger/crc/crc.pri")

SOURCES += main.cpp \
    ccrc16_test.cpp

HEADERS += \
    ccrc16_test.h
