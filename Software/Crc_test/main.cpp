#include <QCoreApplication>
#include <QTest>
#include "ccrc16_test.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    QTest::qExec(new cCrc16_test(), argc, argv);

    return a.exec();
}
