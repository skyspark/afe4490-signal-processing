#include "ccrc16_test.h"

#include "crc16.h"
#include <QTest>

cCrc16_test::cCrc16_test(QObject *parent) : QObject(parent)
{

}

void cCrc16_test::test()
{
    QByteArray ba;

    ba.append("\x00\x06xxTEST");
    quint16 crc = crc16_update_blk(0xFFFF, (uint8_t *)ba.data(), 8);
    QCOMPARE(crc, (uint16_t)(11638));
}
