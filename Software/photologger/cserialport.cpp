#include "cserialport.h"
#include "QDebug"
#include "QtGui"
#include "QMessageBox"


CSerialPort::CSerialPort(QObject *parent) : QObject(parent)
   {
   }

CSerialPort::~CSerialPort()
   {
    emit finished_Port();//Сигнал о завершении работы
}

void CSerialPort::portConnection(bool setConnection)
{
    if( setConnection ) ConnectPort();
    else DisconnectPort();
}

void CSerialPort::DisconnectPort()
   {
   qDebug() << "DisConnect!!!";
   //Отключаем порт
   if (thisPort.isOpen()){
      thisPort.close();
      error_(SettingsPort.name.toLocal8Bit() + " >> Close!");
      }
   }

void CSerialPort::ConnectPort()
   {
   qDebug() << "Port" << QThread::currentThreadId();
   qDebug()<< "Connect to port: " << SettingsPort.name;
   //процедура подключения
   thisPort.setPortName(SettingsPort.name);
   if (thisPort.open(QIODevice::ReadWrite)) {
      if (thisPort.setBaudRate(SettingsPort.baudRate)
          && thisPort.setDataBits(SettingsPort.dataBits)//DataBits
          && thisPort.setParity(SettingsPort.parity)
          && thisPort.setStopBits(SettingsPort.stopBits)
          && thisPort.setFlowControl(SettingsPort.flowControl))
         {
         if (thisPort.isOpen()){
            error_((SettingsPort.name+ " >> Open!").toLocal8Bit());
            }
         } else {
         thisPort.close();
         error_(thisPort.errorString().toLocal8Bit());
         }
      } else {
      thisPort.close();
      error_(thisPort.errorString().toLocal8Bit());
      }
   }

void CSerialPort::process_Port()
   {
   //Выполняется при старте класса
   connect(&thisPort,SIGNAL(error(QSerialPort::SerialPortError)), this, SLOT(handleError(QSerialPort::SerialPortError))); // подключаем проверку ошибок порта
   connect(&thisPort, SIGNAL(readyRead()),this,SLOT(ReadInPort()));//подключаем   чтение с порта по сигналу readyRead()
   }

void CSerialPort::WriteToPort(QByteArray data)
   {
   //Запись данных в порт
   if (thisPort.isOpen()){
      thisPort.write(data);
      }
   }

void CSerialPort::handleError(QSerialPort::SerialPortError error)
   {
   if ((thisPort.isOpen()) && (error == QSerialPort::ResourceError)) {
      error_(thisPort.errorString().toLocal8Bit());
      DisconnectPort();
      }
   }

void CSerialPort::ReadInPort()
   {
   //Чтение данных из порта
   QByteArray data(thisPort.readAll());
   emit outPort(data);
   }

