#ifndef CSERIALPORT_H
#define CSERIALPORT_H

#include <QObject>
#include <QSerialPort>//Обьявляем работу с портом
#include <QSerialPortInfo>

/** ------------------------------------------------------------------------------------------------
 * @brief The Settings struct - Структура с настройками порта
 */
struct Settings
   {
   QString                     name;          ///@< Имя
   qint32                      baudRate;      ///@< Скорость
   QSerialPort::DataBits       dataBits;      ///@< Бит в кадре
   QSerialPort::Parity         parity;        ///@< Четность
   QSerialPort::StopBits       stopBits;      ///@< Кол-во стоповых бит
   QSerialPort::FlowControl    flowControl;   ///@< Контроль потока
   };

/** ------------------------------------------------------------------------------------------------
 * @brief The CSerialPort class - класс последовательного порта
 */
class CSerialPort : public QObject
   {
   Q_OBJECT
public:
   explicit CSerialPort(QObject *parent = 0);
   ~CSerialPort();
   QSerialPort thisPort;
   Settings SettingsPort;
signals:
   void finished_Port();
   void error_(QString err);
   void outPort(QByteArray data);
public slots:
   void portConnection( bool setConnection );
   void DisconnectPort();
   void ConnectPort(void);
   void process_Port();
   void WriteToPort(QByteArray data);
private slots:
   void setName(QString n){ SettingsPort.name = n; }
   void handleError(QSerialPort::SerialPortError error);
   void ReadInPort();
   };

#endif // CSERIALPORT_H
