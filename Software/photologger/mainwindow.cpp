#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "cplotter.h"
#include "cserialport.h"
#include "cppgcontrol.h"

int funcTest(int a){
   return a+1;
   }


MainWindow::MainWindow(QWidget *parent) :
   QMainWindow(parent), ui(new Ui::MainWindow), appSettings("PRFL", "ppgLogger")
   {

   // PROTOCOL PARSER
   ui->setupUi(this);
   bl_in = new CBLiteDecode;

   // SERIAL OBJECT
   pSerial = new QSerialPort();

   // LOAD PREVISION SETTINGS
   loadSettings();

   // POINTERS TO GRAPH WINDOWS
   wdtCSpO2.clear();
   wdtPlotter.clear();

   //   qDebug() << this;

   for(int i=0; i<COUNT_AFE_REG; ++i)
      {
      ppg_control.Regs.raw[i]=0;
      }

   sendingDelayTimer = new QTimer;
   sendingDelayTimer->setInterval(600);
   sendingDelayTimer->setSingleShot(true);
   sendingDelayTimer->start();
   responseOutTimer = new QTimer;
   responseOutTimer->setInterval(100);
   responseOutTimer->setSingleShot(true);
   responseOutTimer->start();

   linker = new LinkerGUI();
   linker->init(":/config/AFERegs.ini");
   getWidgets(linker->widgets);
   appendSpecialFunctions();
   linker->setWindow(this);
   linker->setRegs(&(this->ppg_control.Regs));
   linker->startFunc();

   // STATTUS BAR COMPONENTS
   pInfoLb         = new QLabel;
   pFolderInfoLb   = new QLabel;
   pFileSizeLb     = new QLabel;
   pFolderInfoLb->setText("");
   pFileSizeLb->setText("");
   ui->statusBar->addWidget(pInfoLb);
   ui->statusBar->addWidget(pFolderInfoLb);
   ui->statusBar->addWidget(pFileSizeLb);
   QTimer* t = new QTimer;
   t->setInterval(1000);
   // SHOW PACKET COUNTERS
   connect(t, &QTimer::timeout, [=]()
   {
      pInfoLb->setText(QString("pkt:%1 \t err:%2 (events per sec)").arg(pktcnt).arg(errcnt));
      pktcnt = 0; errcnt = 0;
      });
   t->start();

   // SERIAL PORT THREAD
   QThread *thread_New = new QThread;              //Создаем поток для порта платы
   //QThread *thread_BL  = new QThread;            //Создаем поток для обработчика протокола
   CSerialPort *PortNew = new CSerialPort();       //Создаем обьект по классу
   PortNew->moveToThread(thread_New);              //помешаем класс  в поток
   PortNew->thisPort.moveToThread(thread_New);     //Помещаем сам порт в поток
   bl_in->moveToThread(thread_New);

   // SCAN AVALIBLE SERIAL PORT
   QList<QSerialPortInfo> ports = QSerialPortInfo :: availablePorts();
   QStringList names;

   names.append("ttyUSB0");
   QSerialPortInfo port;
   int i = 0, idx = 0;
   foreach (port, ports)
      {
      names << (port.portName());
      if (port.portName() == pSerial->portName()) { idx = i; }
      ++i;
      // qDebug() << port.portName();
      }
   ui->comboBox->addItems(names);
   ui->comboBox->setCurrentIndex(idx);

   connect(ui->comboBox, SIGNAL(currentIndexChanged(QString)), PortNew, SLOT(setName(QString)));
   emit ui->comboBox->currentIndexChanged(names[idx]);
   // ---------------------------------------------------------------------------------------------------
   // НАСТРОЙКИ ПОРТА
   PortNew->SettingsPort.baudRate      = 500000;
   PortNew->SettingsPort.dataBits      = QSerialPort::Data8;
   PortNew->SettingsPort.flowControl   = QSerialPort::NoFlowControl;
   PortNew->SettingsPort.name          = names[0];
   PortNew->SettingsPort.parity        = QSerialPort::NoParity;
   PortNew->SettingsPort.stopBits      = QSerialPort::OneStop;

   //   QList<QSerialPortInfo> ports = QSerialPortInfo :: availablePorts();

   connect(PortNew, SIGNAL(error_(QString)), this, SLOT(error_print(QString)));                     // Лог ошибок
   connect(thread_New, SIGNAL(started()), PortNew, SLOT(process_Port()));                           // Переназначения метода run
   connect(PortNew, SIGNAL(finished_Port()), thread_New, SLOT(quit()));                             // Переназначение метода выход
   connect(thread_New, SIGNAL(finished()), PortNew, SLOT(deleteLater()));                           // Удалить порт
   connect(PortNew, SIGNAL(finished_Port()), thread_New, SLOT(deleteLater()));                      // Удалить поток
   connect(ui->actionStart, SIGNAL(triggered( bool )),PortNew,SLOT(portConnection( bool )));        // по нажатию кнопки подключить/отключить порт
   connect(this, SIGNAL(sendSerial(QByteArray)), PortNew, SLOT(WriteToPort(QByteArray)));           // отправка байтов
   connect(PortNew, SIGNAL(outPort(QByteArray)), bl_in, SLOT(slotData(QByteArray)));                // прием байтов
   connect(bl_in, SIGNAL(outdata(QByteArray)), this, SLOT(slotPkt(QByteArray)));                    // по успешному разбору пакета
   connect(thread_New, SIGNAL(finished()), bl_in, SLOT(deleteLater()));                             // Удалить обработчик
   connect(ui->sbLED1, SIGNAL(valueChanged(int)), SLOT(on_cbLEDRANGEChanged(int)));                 // Корректировка значений контролов
   connect(ui->sbLED2, SIGNAL(valueChanged(int)), SLOT(on_cbLEDRANGEChanged(int)));                 // Корректировка значений контролов
   connect(ui->cbLEDRANGE, SIGNAL(currentIndexChanged(int)), SLOT(on_cbLEDRANGEChanged(int)));      // Корректировка значений контролов
   connect(ui->cbTX_REF, SIGNAL(currentIndexChanged(int)), SLOT(on_cbLEDRANGEChanged(int)));        // Корректировка значений контролов

   connect(linker, (void(LinkerGUI::*)(void))&LinkerGUI::regsChanged, this, [=]() {prepareSending(SF_hasUpdatedRegs); } );
   connect(this, &MainWindow::responseRecieved, this, [=]() {prepareSending(SF_hasResponse); } );
   connect(this->responseOutTimer, &QTimer::timeout, this, [=]() {prepareSending(SF_hasResponseTimeOut); } );
   connect(this->sendingDelayTimer, &QTimer::timeout, this, [=]() {prepareSending(SF_hasExpiredSendingDelay); } );
   connect(this->linker, (void(LinkerGUI::*)(BitAdress))&LinkerGUI::regsChanged, this, &MainWindow::printTableCell);

   // Отправка значений регистров при изменении с GUI
   connect(ui->actionRead, SIGNAL(triggered()), this , SLOT(sendAllRegistersResponse()) );        // Отправка запроса на чтение всех регистров
   connect(ui->actionInit, SIGNAL(triggered()), this, SLOT(sendInitAfeResponse()) );                // Отправка запроса на инит AFE
   connect(ui->actionStream ,SIGNAL(triggered(bool)), this, SLOT(sendStreamAfeResponse(bool)));    // Разрешение отправки потоковых данных от AFE
   connect(ui->actionStartProcessing ,SIGNAL(triggered(bool)), this, SLOT(sendStreamAfeResponse(bool)));

   on_cbLEDRANGEChanged(0);

   // START
   thread_New->start(QThread::NormalPriority);
   Log << good("Starting.");
   }

void MainWindow :: loadSettings(void)
   {
   appSettings.beginGroup("/Settings");
   if (pSerial != NULL)
      pSerial->setPortName(appSettings.value("/comname", "ttyUSB0").toString());
   folderName = appSettings.value("/logfolder", "C:\\temp\\").toString();
   folderNameCfg = appSettings.value("/cfgfolder", "C:\\temp\\").toString();
   appSettings.endGroup();
   }

void MainWindow::saveSettings(void)
   {
   appSettings.beginGroup("/Settings");
   if (pSerial != NULL)
      appSettings.setValue("/comname", pSerial->portName());
   appSettings.setValue("/logfolder", folderName);
   appSettings.setValue("/cfgfolder", folderNameCfg);
   appSettings.endGroup();
   }


void MainWindow::prepareSending(sendFlags e)
   {
   flags[e] = true;

   if (flags[SF_hasExpiredSendingDelay] == true &&
       flags[SF_hasUpdatedRegs] == true &&
       (flags[SF_hasResponseTimeOut] == true ||
        flags[SF_hasResponse] == true)){
      sendSettings();
      for(int i=0; i<4; ++i) {
         flags[i]=false;
         }
      sendingDelayTimer->start();
      responseOutTimer->start();
      }
   }

void MainWindow::sendSettings()
   {
   writeCfgRegs( ppg_control.Regs.raw , ui->sbStartAdr->value() , ui->sbCount->value() );
   }

void MainWindow::writeCfgRegs( uint24_t* regs , uint8_t start, uint8_t count )
   {
   QByteArray payLoad;
   payLoad.append( start );
   payLoad.append( count );
   for( int i = start ; i < start + count ; i++ ) payLoad.append( ( const char* )( uint8_t* )regs[ i ] , sizeof( uint24_t ));
//   Log << payLoad;
   emit sendSerial( CBLiteDecode :: Write( 'W', payLoad , payLoad.length() ) );
   }

void MainWindow::sendShortCommand(char comm , uint8_t start, uint8_t stop)
   {
   QByteArray payLoad; payLoad.append( start ); payLoad.append( stop );
   emit sendSerial( CBLiteDecode :: Write( comm, payLoad , 2 ) );
   }

MainWindow::~MainWindow()
   {
   saveSettings();
   delete ui;
   }

void MainWindow::getWidgets(QList<QWidget*> &widgets)
   {
   widgets = this->findChildren<QWidget*>();
   }

void MainWindow::error_print(QString serr) { Log << error(serr); }

void MainWindow::Print(QString str){ Log << str; }

void MainWindow::on_tableWidget_cellChanged(int row, int column)
   {
//   if (column==0){
//      QModelIndex valueCell = ui->tableWidget->model()->index(row,0);
//      int value = ui->tableWidget->model()->data(valueCell).toString().toInt(0,16);
//      linker->writeRegister(BitAdress(row),value, false);
//      }
   }

void MainWindow::appendSpecialFunctions()
   {
   linker->appendSpecialMethodsHash(QString("TIMING"),&MainWindow::setTiming);
   }

void MainWindow::slotPkt(QByteArray arr)
   {
   // Глобальный счетчик принятых пакетов
   char* pdu = arr.data();
   pktcnt++;
   // Разбираем команду
   if (pdu[0] == 'X') { emit putData(arr); if (file.isOpen()) fileWriteCSV(arr); return; }
   if (pdu[0] == 'A') { Log << good(QString("answer ok: %1; %2").arg((uint)(pdu[1])).arg((uint)(pdu[2]))); emit responseRecieved(); return; }
   if (pdu[0] == 'R') { memcpy( ppg_control.Regs.raw+1, pdu+3, COUNT_AFE_REG*3 ); }
   if (pdu[0] != 'R') { Log << error(QString("unknow command: %1").arg(pdu[0])); return; }

   return;
   }

void MainWindow::on_actionRead_triggered()
   {
   //unsigned char start = (unsigned char) ui->sbStartAdr->value();
   //if (start == 0) start = 1;
   //ui->sbStartAdr->setValue(start);
   //unsigned char buf[] = { start, (unsigned char)ui->sbCount->value()  };
   //   QByteArray pkt = CBLiteDecode::Write('R', buf, 2);
   //   emit sendSerial(pkt);
   }

/// TODO: del me plzzz
void MainWindow::on_actionWrite_triggered()
   {
   //      unsigned char start = (unsigned char) ui->sbStartAdr->value();
   //      if (start == 0) start = 1;
   //      ui->sbStartAdr->setValue(start);
   //      unsigned char count = (unsigned char) ui->sbCount->value();
   //      ui->tableWidget->setDisabled(false);

   //      unsigned char pl[(MAX_AFE_REG_NUM + 2) * 4] = { start, count };
   //      ppg_control.getReg((uint32_t*)(pl + 2), start, count);
   //      printTableItem(start, count, (uint32_t*)(pl + 2));
   }

void MainWindow::on_actionStartProcessing_triggered(bool checked)
   {
   unsigned int buf = checked ? 1 : 0;

   //   QByteArray pkt = CBLiteDecode::Write('S', (unsigned char*)&buf, 2);
   //emit sendSerial(pkt);

   if (!checked)
      {
      if (proc)
         {
         disconnect(this, SIGNAL(putData(const QByteArray&)), proc, SLOT(addPoint(const QByteArray &)));
         delete proc;
         }
      return;
      }
   proc = new Processing();
   proc->show();
   connect(this,SIGNAL(MainClose()), proc, SLOT(deleteLater()));
   connect(this, SIGNAL(putData(const QByteArray&)), proc, SLOT(addPoint(const QByteArray &)));                                  //по приему измерения от устройства
   connect((QObject*) proc, &QObject::destroyed, [this](){ ui->actionStartProcessing->setChecked(false); });
   return;
   }

void MainWindow::on_actionStream_triggered(bool checked)
   {
   unsigned int buf = checked ? 1 : 0;

   //   QByteArray pkt = CBLiteDecode::Write('S', (unsigned char*)&buf, 2);
   //emit sendSerial(pkt);

   if (!checked)
      {
      if (wdtPlotter)
         {
         disconnect(this, SIGNAL(putData(const QByteArray&)), wdtPlotter, SLOT(addPoint(const QByteArray &)));
         wdtPlotter->deleteLater();
         }
      return;
      }
   wdtPlotter = new cplotter();
   wdtPlotter->show();
   connect(this,SIGNAL(MainClose()), wdtPlotter, SLOT(deleteLater()));
   connect(this, SIGNAL(putData(const QByteArray&)), wdtPlotter, SLOT(addPoint(const QByteArray &)));                                  //по приему измерения от устройства
   connect((QObject*) wdtPlotter, &QObject::destroyed, [this](){ ui->actionStream->setChecked(false); });
   return;
   }

/** ------------------------------------------------------------------------------------------------------------------------
 * @brief MainWindow::on_cbTX_REF_currentIndexChanged
 * @param index
 *
 * LED current will be = (LED[7:0] / 256) x Full Scale
 */
void MainWindow::on_cbLEDRANGEChanged(int)
   {
   qreal  maxCurr[] = { 75, 50, 100, 0 };
   if (ui->cbTX_REF->currentIndex() > 4) ui->cbTX_REF->setCurrentIndex(4);
   int index = ui->cbTX_REF->currentIndex();
   QString val1, val2;
   val1 = QString("%1mA").arg(maxCurr[index]);
   val2 = QString("%2mA").arg(maxCurr[index] * 2);

   //index = ui->cbLEDRANGE->currentIndex();
   ui->cbLEDRANGE->setItemText(0, val2);
   ui->cbLEDRANGE->setItemText(1, val1);
   ui->cbLEDRANGE->setItemText(2, val2);

   double fs = (ui->cbLEDRANGE->currentIndex() >= 3) ? 0 : maxCurr[ui->cbTX_REF->currentIndex()];
   if (ui->cbLEDRANGE->currentIndex() != 1) fs = fs * 2; fs = fs / 256;

   currLED[0] = ui->sbLED1->value() * fs;
   currLED[1] = ui->sbLED2->value() * fs;

   ui->lbLEDCurrent->setText(QString("LED1 = %1mA  \t  LED2 = %2mA").arg(currLED[0], 0, 'g',3).arg(currLED[1], 0, 'g',3));
   }

QList<QPair<BitAdress,int> > MainWindow::setTiming(int a)
   {
   QList<QPair<BitAdress,int> > list;
   ppg_control.set_timing(list, ui->sbdutyTIMING->value(), ui->sbfreqTIMING->value());
   return list;
   }

void MainWindow::on_actionSetFileName_triggered()
   {
   QFileDialog* fd = new QFileDialog(this, "Set log folder", folderName);
   fd->setFileMode(QFileDialog::DirectoryOnly);
   if (fd->exec()) folderName = fd->selectedFiles().first();
   pFolderInfoLb->setText(folderName);
   delete(fd);
   }

void MainWindow::on_actionCapture_triggered(bool checked)
   {
   if (!checked)
      {
      if (file.isOpen()) file.close();
      pFolderInfoLb->setText(folderName);
      pFileSizeLb->setText("");
      }
   else
      {
      fileSize = 0;
      QString str = generateName("ppg");
      pFolderInfoLb->setText(str);
      file.setFileName(str);
      if (!file.open(QFile::WriteOnly)) { ui->actionCapture->toggle(); return; }
      file.write(QByteArray("Red \t RedAmb \t IR \t IRAmb \t RedDiff \t IRDiff \r\n"));
      }
   }

void MainWindow::fileWriteCSV(QByteArray arr)
   {
//   uint32_t* pData = ( uint24_t* )( arr.data()+ 1 );
//   QString str = "";
//   int cnt = arr.count() / 4 - 1;
//   for(int i = 0 ; i < cnt ; i++)
//      {
//      str += QString("%1").arg(pData[i]);
//      if (i == cnt - 1) str += "\r\n"; else str += "\t";
//      }
//   file.write(QByteArray(str.toLatin1()));
//   fileSize += str.count();
//   pFileSizeLb->setText(QString("writed: %1 kbytes").arg(fileSize / 1024));
   }

void MainWindow::printTableCell(BitAdress adr)
   {
   int reg = linker->getRegister(adr.index);
   ui->tableWidget->setItem(adr.index, 0, new QTableWidgetItem(QString::number(reg & 0x00FFFFFF, 16)));
   }

void MainWindow::printTable(QStringList strList)
   {
   ui->tableWidget->setRowCount(strList.last().split('\t').at(0).toInt(0,16)+1);
   ui->tableWidget->setColumnCount(2);
   ui->tableWidget->clearContents();

   QStringList names = strList.at(0).split('\t');
   names.removeFirst();
   ui->tableWidget->setHorizontalHeaderLabels( names );

   for(uint16_t i = 1 ; i < strList.length() ; i++)
      {
      QStringList strColumns = strList.at(i).split('\t');
      if (strColumns.length()<3) continue;
      int index = strColumns.at(0).toInt(0,16);
      ui->tableWidget->setVerticalHeaderItem(index, new QTableWidgetItem(QString("0x%1").arg(index, 2, 16, QChar('0'))));
      ui->tableWidget->setItem(index, 0, new QTableWidgetItem(strColumns.at(1)));
      ui->tableWidget->setItem(index, 1, new QTableWidgetItem(strColumns.at(2)));
      if (i==MAX_AFE_REG_NUM+1)
         break;
      }
   ui->tableWidget->horizontalHeader()->setSectionResizeMode(0, QHeaderView::ResizeToContents);
   ui->tableWidget->horizontalHeader()->setSectionResizeMode(1, QHeaderView::Stretch);
   }

void MainWindow::on_actionSaveToFile_triggered()
   {
   QString fileName;
   QFileDialog fileDialog;


   if (ui->tableWidget->rowCount() < 41)
      {
      Log << error("No values in registers table!");
      return;
      }

   fileName = fileDialog.getSaveFileName(this, tr("Save config file"), folderNameCfg, tr("CSV (*.csv)"));
   QFile f (fileName);
   QString str;
   if (!f.open(QFile :: WriteOnly | QFile :: Truncate))
      {
      Log << error("Write file error!");
      return;
      }
   f.write("Addr\tValue\tName\r\n");
   for(int i = 0 ; i < ui->tableWidget->rowCount() ; i++)
      {
      str = QString("%1\t%2\t%3\r\n")
            .arg(i + 1, 2, 16)
            .arg(ui->tableWidget->item(i, 1)->text())
            .arg(ui->tableWidget->item(i, 0)->text());
      f.write(str.toLatin1());
      }
   f.close();
   QFileInfo fileInfo = QFileInfo( fileName );
   folderNameCfg = fileInfo.absolutePath();
   saveSettings();
   Log << good("Config registers saved.");
   }

void MainWindow::on_actionRead_Conf_triggered()
   {
   QString fileName;
   QFileDialog fileDialog;
   loadSettings();
   fileName = fileDialog.getOpenFileName(this, tr("Open config file"), folderNameCfg, tr("CSV (*.csv)"));
   QFile f (fileName);

   if (!f.open(QFile :: ReadOnly)) {
      Log << error("Open file error!");
      return;
      }
   QStringList inputStrings = QString(f.readAll()).split('\n',QString::SkipEmptyParts);
   f.close();

   int rowCount  = inputStrings.count() - 1;

   for(auto str : inputStrings){
      bool valid1, valid2;
      QStringList a = str.split('\t');
      int index = a.at(0).toInt(&valid1, 16);
      uint32_t value = a.at(1).toInt(&valid2,16);
      if (!(valid1 || valid2)){
         Log << error(QString ("Wrong data at line: %1").arg(str));
         continue;
         }
      BitAdress adr(index);
      linker->writeRegister(adr,value);
      if (index==48)
         break;
      }
   Log << good(QString("Loaded %1 rows").arg(rowCount));
   printTable(inputStrings);

   QFileInfo fileInfo = QFileInfo( fileName );
   folderNameCfg = fileInfo.absolutePath();
   saveSettings();
   }

void MainWindow::on_actionSpO2_triggered(bool checked)
   {
   if (checked && wdtCSpO2.isNull())
      {
      wdtCSpO2 = new cSpO2;
      connect(this, SIGNAL(putData(const QByteArray&)), wdtCSpO2, SLOT(addData(const QByteArray &)),Qt::UniqueConnection);
      wdtCSpO2->show();
      }

   if (!checked && !wdtCSpO2.isNull())
      {
      disconnect(this, SIGNAL(putData(const QByteArray&)), wdtCSpO2, SLOT(addData(const QByteArray &)));
      wdtCSpO2->close();
      wdtCSpO2.clear();
      }
   }

double fcCalc(uint32_t Ridx, uint32_t Cf)
   {
   double r[] = {5E5, 2.5E5, 1E5, 5E5, 2.5E4, 1E4, 1E6, -1.0 };
   double c = (double)Cf;

   return 1.0 / (r[Ridx] * c * 1E-12 *  10.);
   }

void MainWindow::printFc()
   {
   double fc1 = fcCalc(ui->cbRF_LED1->currentIndex(), ui->sbCF_LED1->value());
   double fc2 = fcCalc(ui->cbRF_LED2->currentIndex(), ui->sbCF_LED2->value());
   ui->lbFreqc->setText(QString("fc(red): %1 Hz \t fc(ir): %2 Hz")
                        .arg((fc1 < 0)?(-1): fc1, 0, 'f', 0)
                        .arg((fc2 < 0)?(-1): fc2, 0, 'f', 0));
   }

void CBLiteDecode::slotRead()
   {
   QIODevice* dev = (QIODevice*) sender();
   if (dev == NULL) return;

   data = dev->readAll();
   pkt_parser();
   }

