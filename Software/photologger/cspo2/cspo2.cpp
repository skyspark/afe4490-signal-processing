#include "cspo2.h"
#include "ui_cspo2.h"
#include <QDebug>

cSpO2::cSpO2(QWidget *parent) :
   QWidget(parent),
   ui(new Ui::cSpO2)
   {
   counter = 0;
   red.reset(); ir.reset();
   ui->setupUi(this);

   ui->plotAC->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom | QCP::iSelectAxes |
                               QCP::iSelectLegend | QCP::iSelectPlottables);
   ui->plotSpO2->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom | QCP::iSelectAxes |
                                 QCP::iSelectLegend | QCP::iSelectPlottables);

   ui->plotAC->addGraph();
   ui->plotAC->graph()->setName("red");
   ui->plotAC->graph()->setPen(randomPen());
   ui->plotAC->graph()->setAdaptiveSampling(true);

   ui->plotAC->addGraph();
   ui->plotAC->graph()->setName("ir");
   ui->plotAC->graph()->setPen(randomPen());
   ui->plotAC->graph()->setAdaptiveSampling(true);
   ui->plotAC->axisRect()->setupFullAxesBox();

   ui->plotSpO2->addGraph();
   ui->plotSpO2->graph()->setName("ir");
   ui->plotSpO2->graph()->setPen(randomPen());
   ui->plotSpO2->graph()->setAdaptiveSampling(true);
   ui->plotSpO2->axisRect()->setupFullAxesBox();

   ui->plotAC->legend->setVisible(true);
   ui->plotSpO2->legend->setVisible(true);


   QTimer* pTimer = new QTimer;
   connect(pTimer, &QTimer::timeout, this, [=](){
      if (ui->plotAC->graphCount() != 2) return;
      ui->plotAC->graph(0)->removeDataBefore(t - 100.0);
      ui->plotAC->graph(1)->removeDataBefore(t - 100.0);

      ui->plotAC->xAxis->setRange(t + 10.0, 100.0, Qt::AlignRight);
      ui->plotAC->replot();

      if (ui->plotSpO2->graphCount() != 1) return;
      ui->plotSpO2->graph(0)->removeDataBefore(t - 100.0);
      ui->plotSpO2->xAxis->setRange(t + 10.0, 100.0, Qt::AlignRight);
      ui->plotSpO2->replot();


      });
   connect(this, SLOT(close()), pTimer, SLOT(deleteLater()));
   pTimer->start(100);
   }

cSpO2::~cSpO2()
   {
   delete ui;
   }
/**
 * @brief cSpO2::addData
 */
void cSpO2::addData(const QByteArray &arr)
   {
   uint32_t* pData = (uint32_t*)(arr.data()+ 1);

   if (arr.count() < 5*4) return;

   double diffRED = (double) pData[4]  / AFE_ADC_MAX;
   double diffIR  = (double) pData[5]  / AFE_ADC_MAX;

   red += diffRED;
   ir+= diffIR;
   ++counter;

   counter2++;

   uint32_t n = fifoRED.order();
   t = (double)counter / n;
   fifoRED(diffRED);
   fifoIR (diffIR);

   fp_t R_RED = fifoRED.sigma() / fifoRED.mean();
   fp_t R_IR  = fifoIR.sigma() / fifoIR.mean();


   ui->plotAC->graph(0)->addData(t, R_RED);
   ui->plotAC->graph(1)->addData(t, R_IR );

   }
