#ifndef CIIR_H
#define CIIR_H

#include "lptypes.h"

/** ---------------------------------------------------------------------------------------------------------
 * @brief Структура коэффициентов БИХ-фильтра
 */
typedef struct IIR_COEFF_S
{
    fp_t  a;
    fp_t  b;
} iir_coeff_t;

/** ---------------------------------------------------------------------------------------------------------
 * @brief Шаблон очереди для БИХ-фильтра
 * ORDER - порядок
 */
template< uint8_t ORDER >
class ciir_fifo
{
public:
    /**
     * @brief cirr_fifo - конструктор и деструктор
     * @{
     */
    ciir_fifo(){ reset(); }
    ~ciir_fifo() { }
    /** @} */
    /**
     * @brief operator () - добавление значения
     * @param in - значение
     */
    void operator()(const fp_t in)
    {
        ARRAY[end] = in;
        if (++end >= ORDER) end = 0;
        if (begin == end)
            if (++begin >= ORDER) begin = 0;
    }
    /**
     * @brief operator [] - извлечение из очереди по индексу
     * @param index - индекс
     * @return - значение
     */
    fp_t operator[](const uint32_t index)
    {
        int i = end - index - 1;
        if (i < 0) i += ORDER;
        return ARRAY[i];
    }
    /**
     * @brief reset - сброс
     */
    void reset() { begin = end = 0; for(int i = 0 ; i < ORDER ; i++) ARRAY[i] = 0.0; }

    fp_t summ() { fp_t res = 0; for(int i = 0 ; i < ORDER ; i++) res += ARRAY[i]; return res; }
    fp_t mean() { return summ() / (fp_t)ORDER; }
    fp_t sigma()
    {
        fp_t res = 0.0;
        fp_t m = mean();
        for(int i = 0 ; i < ORDER ; i++)
        {
            fp_t d = ARRAY[i] - m;
            res += d * d;
        }
        return sqrt(res / ORDER);
    }

    uint32_t order(){ return ORDER; }

private:
    fp_t        ARRAY[ORDER];
    uint32_t    begin, end;
};

/** ---------------------------------------------------------------------------------------------------------
 * @brief Шаблон БИХ-фильтра
 * ORDER - порядок
 * COEFF - указатель на массив коэффициентов
 */
template< uint8_t ORDER, iir_coeff_t const* COEFF >
class ciir
{
public:
    /**
     * @brief ciir - конструктор
     */
    ciir(){ reset(); }
    /**
     * @brief operator () ввод  и обработка значения
     * @param in - входное значение
     * @return - текущее значение выхода
     */
    fp_t operator()(fp_t in){ process(in);  return Y[0]; }
    /**
     * @brief operator () - чтение текущего значения
     * @return - текущее значение выхода
     */
    fp_t operator()(void){ return Y[0]; }
    /**
     * @brief operator ! - проверка установки значения выхода
     * @return - значение установилось
     */
    bool operator!(){ return count >= ORDER ; }
    /**
     * @brief reset - сброс фильтра
     * @param in - установка текущего значения
     */
    void reset() { count = 0; X.reset(); Y.reset(); }

    uint32_t getcount(){ return count; }
    /**
     * Деструктор
     */
    ~ciir(){}
private:
    /**
     * @brief count - счетчик итераций
     */
    uint32_t    count;
    /**
     * @brief ready - флаг установившегося значения
     */
    bool        ready;
    /**
     * @brief current - текущее значение
     */

    /**
     * @brief X, Y - очереди значений X и Y
     */
    ciir_fifo<ORDER>        X;
    ciir_fifo<ORDER-1>      Y;
    /**
     * @brief process - функция обработки значения
     * X.push(in)
     * Y[0] = A[0] * X[0] + { A[n] * X[n] + B[n] * Y[n - 1] }, где n = 1..N
     * @param in - вход
     */
    void        process(fp_t in)
    {
        // Ввод нового значения
        ++count;
        X(in);

        // Вычисление
        //
        fp_t current = X[0] * COEFF[0].a;
        //qDebug() << X[0] << " * " << COEFF[0].a ;
        for(int i = 1 ; i < ORDER  ; i++)
        {
            //qDebug() << X[i] << " * " << COEFF[i].a << " + " << Y[i - 1] << " * " << COEFF[i].b;
            current += X[i] * COEFF[i].a - Y[i - 1] * COEFF[i].b;
        }
        Y(current);
    }
};

#endif // CIIR_H
