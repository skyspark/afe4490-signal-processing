HEADERS += \
    cspo2/cspo2.h \
    cspo2/cstat.h \
    cspo2/lptypes.h \
    cspo2/ciir.h

SOURCES += \
    cspo2/cspo2.cpp\
    cspo2/ciir.cpp


FORMS += \
        cspo2/cspo2.ui

