#ifndef CSPO2_H
#define CSPO2_H

#include <QWidget>
#include "cstat.h"
#include "lptypes.h"
#include "ciir.h"
#include <QPen>

namespace Ui {
class cSpO2;
}

#define AFE_ADC_MAX     16777216.0

class cSpO2 : public QWidget
{
    Q_OBJECT

public:
    explicit cSpO2(QWidget *parent = 0);
    ~cSpO2();
public slots:
    void addData(const QByteArray &arr);

private:
    Ui::cSpO2 *ui;
    cstat  red, ir;
    uint32_t    counter, counter2;

    ciir_fifo<50>   fifoRED, fifoIR;

    double t;

    QPen randomPen()
    {
        QPen graphPen;
        graphPen.setColor(QColor(rand() % 245 + 10, rand() % 245 + 10, rand() % 245 + 10));
        graphPen.setWidthF(rand()/(double)RAND_MAX * 2 + 1);
        return graphPen;
    }
};



#endif // CSPO2_H
