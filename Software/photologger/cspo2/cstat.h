#ifndef CSTAT
#define CSTAT


/** --------------------------------------------------------------------------------------
 * Класс сбора статистики
 */
class cstat
{
public:
    /// КОНСТРУКТОР
    cstat(){ reset(); }
    /// Чтение статистики
    int operator()(double& mean, double& sigma)
    {
        mean  = Xm  / num;

        sigma = Xm * Xm;
        sigma = sigma / num;
        sigma = Xm2 - sigma;
        sigma = sigma / (num - 1);
        sigma = sqrt(sigma);
        return num;
    }
    /// Добавление новой точки
    int operator+=(const double X)
    {
        Xm  = Xm  + X;
        double X2 = X;
        Xm2 = Xm2 + X2 * X;
        ++num;
        return num;
    }
    /// Сброс счетчиков
    void reset()
    {
        Xm = 0; Xm2 = 0;
        num = 0;
    }

private:
    double Xm;
    double Xm2;
    double num;
};


#endif // CSTAT

