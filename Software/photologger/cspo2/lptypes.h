#ifndef LPTYPES_H
#define LPTYPES_H

#include <stdint.h>
#include <math.h>
#include <stddef.h>

/** ---------------------------------------------------------------------
 * @brief Настройка типа с плавающей точкой
 */
#define FLOAT_TYPE  double
typedef FLOAT_TYPE  fp_t;



/** ---------------------------------------------------------------------
 * @brief The Quaternion class
 */
template< class TYPE > struct quater
{
    public:
    union
    {
        struct
        {
            TYPE w;
            TYPE x;
            TYPE y;
            TYPE z;
        };
        TYPE raw[4];
    };

    quater()
    {
        w = (TYPE)1;
        x = 0; y = 0; z = 0;
    }

    quater(TYPE nw, TYPE nx, TYPE ny, TYPE nz)
    {
        w = nw; x = nx; y = ny; z = nz;
    }
    /**
     * @brief operator *
     * @param q
     * @return
     */
    quater<TYPE> operator*(quater<TYPE> &q)
    {
        return quater<TYPE>(
            w*q.w - x*q.x - y*q.y - z*q.z,  // new w
            w*q.x + x*q.w + y*q.z - z*q.y,  // new x
            w*q.y - x*q.z + y*q.w + z*q.x,  // new y
            w*q.z + x*q.y - y*q.x + z*q.w); // new z
    }
    /**
     * @brief conjugate - сопряжение
     * @return
     */
    quater<TYPE> conj()
    {
        return quater<TYPE>(w, -x, -y, -z);
    }
    /**
     * @brief magn
     * @return
     */
    TYPE magn()
    {
        return sqrt(w * w + x * x + y * y + z * z);
    }
    /**
     * @brief norm
     * @return
     */
    TYPE norm()
    {
        TYPE m = magn;
        w /= m;
        x /= m;
        y /= m;
        z /= m;
    }
    /**
     * @brief getNorm
     * @return
     */
    quater< TYPE > getNorm()
    {
        quater< TYPE > res(raw);
        return res.norm();
    }
};

/** ---------------------------------------------------------------------------------------------------------
 *@{
 */
typedef quater< uint16_t > quaterint16_t;
typedef quater< uint32_t > quaterint32_t;
typedef quater< float    > quaterfloat;
typedef quater< double   > quaterdbl;
typedef quater< fp_t     > quaterfp;
/** @} */
/** ---------------------------------------------------------------------------------------------------------
 * ШАБЛОН: 3D вектора с различными типа данных координат
 */
template< class TYPE > struct vec
{
    public:
    #define DIM           3
    /**
     * @brief The Anonymous:1 Для хранения и различных представлений
     */
    union
    {
        struct
        {
            TYPE x;
            TYPE y;
            TYPE z;
        };
        TYPE raw[DIM];
    };
#define TYPESIZE    (sizeof(raw))

    vec(TYPE* v) { memcpy(raw, v, TYPESIZE); }
    vec(TYPE ix, TYPE iy, TYPE iz) { x = ix; y = iy; z = iz; }
    vec(TYPE n = 0){ memset(raw, n, TYPESIZE); }

    TYPE operator[](size_t i){ return raw[i]; }

    vec<TYPE> operator=(vec<TYPE> &v)  { memcpy(raw, v, TYPESIZE); return this; }
    vec<TYPE> operator=(TYPE &s)       { memset(raw, s, TYPESIZE); return this; }
    vec<TYPE> operator+=(vec<TYPE> &a) { vec<TYPE> res = *this; for(int i = 0 ; i < DIM ; i++) res.raw[i] += a[i]; return res; }
    vec<TYPE> operator+(vec<TYPE> &a) { vec<TYPE> res = *this; res += a; return res; }
    vec<TYPE> operator-=(vec<TYPE> &a) { vec<TYPE> res = *this; for(int i = 0 ; i < DIM ; i++) res.raw[i] -= a[i]; return res; }
    vec<TYPE> operator-(vec<TYPE> &a) { vec<TYPE> res = *this; res -= a; return res; }
    vec<TYPE> operator*=(TYPE &s)      { vec<TYPE> res = *this; for(int i = 0 ; i < DIM ; i++) res.raw[i] *= s; return res; }
    vec<TYPE> operator*(TYPE &s)      { vec<TYPE> res = *this; res *= s; return res; }
    vec<TYPE> operator/=(TYPE &s)      { vec<TYPE> res = *this; for(int i = 0 ; i < DIM ; i++) res.raw[i] /= s; return res; }
    vec<TYPE> operator/(TYPE &s)      { vec<TYPE> res = *this; res /= s; return res; }
    /**
     * @brief magn - Вычислениие длины
     * @return - длина
     */
    TYPE magn() { return sqrt(x * x + y * y + z * z); }
    /**
     * @brief magn - Нормирование
     * @return - нормированный вектор
     */
    vec<TYPE> norm() { TYPE m = this->magn(); this->operator /=(m); return *this; }
    /**
     * @brief getNorm
     * @return
     */
    vec<TYPE> getNorm() { vec<TYPE> res = *this; return res.norm(); }
    /**
     * Преобразование типа
     */
    template<class TYPEA> operator vec<TYPEA>()
    {
        vec<TYPEA> res;
        for(int i = 0 ; i < DIM ; i++) res[i] = (TYPEA) raw[i];
        return res;
    }
    /**
     * @brief rot
     * @param q
     */
    void rot(quater<TYPE> &q)
    {
        quater<TYPE> p(0, x, y, z);
        // quaternion multiplication: q * p, stored back in p
        p = q * p;
        // quaternion multiplication: p * conj(q), stored back in p
        p = p * (q.conj());
        // p quaternion is now [0, x', y', z']
        x = p.x; y = p.y; z = p.z;
    }

    vec<TYPE> getRot(quater<TYPE> &q)
    {
        quater<TYPE> res(x, y, z);
        res.rot(q);
        return res;
    }
};

/** ---------------------------------------------------------------------------------------------------------
 *@{
 */
typedef vec< uint16_t > vecint16_t;
typedef vec< uint32_t > vecint32_t;
typedef vec< float    > vecfloat;
typedef vec< double   > vecdbl;
/** @} */

/** ---------------------------------------------------------------------------------------------------------
 *
 */
typedef struct SLOPEBIAS_S
{
    fp_t slope[DIM];
    fp_t bias[DIM];
} slope_bias_t;


#endif // LPTYPES_H

