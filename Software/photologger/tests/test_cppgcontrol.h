#ifndef TEST_CPPGCONTROL_H
#define TEST_CPPGCONTROL_H

#include <QObject>

class test_cppgcontrol : public QObject
{
    Q_OBJECT
public:
    explicit test_cppgcontrol( QObject *parent = 0 );
private slots: // должны быть приватными
    /** ----------------------------------------------------
     * @brief getReg
     * int getReg( uint32_t *buf , uint16_t off = 0 , uint16_t len = COUNT_AFE_REG )
     */
    void getsetReg( );

    /** ----------------------------------------------------
     * @brief setControl
     *  ppgcontrol_t &ppg_ctrl
     */
    void setControl();
    /** ----------------------------------------------------
     * @brief set_txstage
     * ppgtxcontrol_t &txctrl
     */
    void set_txstage();
    /** ----------------------------------------------------
     * @brief set_rxstage
     * ppgrxcontrol_t &ampctrl
     */
    void set_rxstage();
    /** ----------------------------------------------------
     * @brief set_timing
     * ppgsample_ctrl_t &ctrl
     */
    void set_timing();
    /** ----------------------------------------------------
     * @brief getControl
     * ppgcontrol_t &ppg_ctr
     */
    void getControl();
    /** ----------------------------------------------------
     * @brief get_txstage
     * ppgtxcontrol_t &txctrl
     */
    void get_txstage();
    /** ----------------------------------------------------
     * @brief get_rxstage
     * ppgrxcontrol_t &ampctrl
     */
    void get_rxstage();
    /** ----------------------------------------------------
     * @brief get_timing
     * ppgsample_ctrl_t &ctrl
     */
    void get_timing();
};

#endif // TEST_CPPGCONTROL_H
