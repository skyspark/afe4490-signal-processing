QT += core testlib
QT -= gui

CONFIG += c++11

TARGET = tests
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

 SUBDIRS += ../ \

SOURCES += main.cpp \
    test_cppgcontrol.cpp \
    ../cppgcontrol.cpp

HEADERS += \
    test_cppgcontrol.h \
    ../cppgcontrol.h
