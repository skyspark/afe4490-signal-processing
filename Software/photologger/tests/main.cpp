#include <QCoreApplication>
#include <QTest>
#include <iostream>
#include <cstdlib>
#include <cstdio>
#include "test_cppgcontrol.h"
using namespace std;

int main(int argc, char *argv[])
{
    freopen("testing.log", "w", stdout);
    QCoreApplication a(argc, argv);
    QTest::qExec(new test_cppgcontrol, argc, argv);
    cout << endl;
    return 0;
}
