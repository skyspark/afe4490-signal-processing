#include <QTest>
#include "test_cppgcontrol.h"
#include "../cppgcontrol.h"

test_cppgcontrol::test_cppgcontrol( QObject *parent ) : QObject(parent){ }

/** ----------------------------------------------------
 * @brief getReg
 * int getReg( uint32_t *buf , uint16_t off = 0 , uint16_t len = COUNT_AFE_REG )
 */
void test_cppgcontrol::getsetReg()
{
    CPPGControl x;
    uint32_t inReg[ COUNT_AFE_REG ], outReg[ COUNT_AFE_REG ];
    memset( inReg , 0x55 , COUNT_AFE_REG * 4 );
    x.setReg( inReg );
    x.getReg( outReg );
    for( int i = 0 ; i < SPARE1 ; i++ )
        QCOMPARE( inReg[ i ] , outReg[ i ] );
    QCOMPARE( outReg[ SPARE1 ], ( uint32_t )0 );
    for( int i = SPARE2 ; i < COUNT_AFE_REG ; i++ )
        QCOMPARE( outReg[ i ] , ( uint32_t )0 );
}

void test_cppgcontrol::setControl()
{
    CPPGControl x;
    CPPGControl::ppgcontrol_t ppg_ctrl =
    {
        /*uint8_t  tmr_reset   */ 1,
        /*uint8_t  tmr_enb     */ 1,
        /*uint8_t  sw_rst      */ 1,
        /*uint8_t  diag_en     */ 1,
        /*uint8_t  pd_rx       */ 1,
        /*uint8_t  pd_tx       */ 1,
        /*uint8_t  slow_diag   */ 1,
        /*uint8_t  xtal_dis    */ 1,
        /*uint8_t  aver  */       4
    };
    x.setControl( ppg_ctrl );
    uint32_t ctrl;
//    x.getReg( &ctrl , CONTROL0 , 1 );
//    QCOMPARE( ctrl , ( uint32_t )0 );
//    x.getReg( &ctrl , CONTROL1 , 1 );
//    QCOMPARE( ctrl , ( uint32_t )0 );
//    x.getReg( &ctrl , CONTROL2 , 1 );
//    QCOMPARE( ctrl , ( uint32_t )0 );

}

void test_cppgcontrol::set_txstage()
{

}

void test_cppgcontrol::set_rxstage()
{

}

void test_cppgcontrol::set_timing()
{

}

void test_cppgcontrol::getControl()
{

}

void test_cppgcontrol::get_txstage()
{

}

void test_cppgcontrol::get_rxstage()
{

}

void test_cppgcontrol::get_timing()
{

}

