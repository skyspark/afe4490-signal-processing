#ifndef QBLITEPROTO_H
#define QBLITEPROTO_H

#include <stddef.h>
#include <stdint.h>
#include <QApplication>
#include <QtCore>
#include "mainwindow.h"


/** -------------------------------------------------------------------------------
 * @brief Длина заголовка PDU <CMD><TS>
 */
#define HDR_PDU_LEN     5


/** ------------------------------------------------------------------------
 * @brief Этапы приема пакета
 */
typedef enum FSM_RX_PKT_E
   {
   FSM_SIGN,
   FSM_LEN,
   FSM_DATA,
   FSM_CRC,
   FSM_VALID,
   } fsm_rx_pkt_t;
/** ------------------------------------------------------------------------
 * @brief Типы данных протокола
 */
typedef enum BL_TYPES_E
   {
   BL_BYTE  = 1,
   BL_SHORT = 2,
   BL_LONG  = 4,
   BL_FLOAT = 5
   }bl_types_t;

struct CBlitePDU
   {
   CBlitePDU(QByteArray& arr) { fromByteArray(arr); }

   void fromByteArray(const QByteArray& arr){
      CMD = arr[0];
      TS  = *(clock_t*)arr.mid(1, HDR_PDU_LEN).data();
      PL  = arr.right(arr.length() - HDR_PDU_LEN);
      }

   void toByteArray(QByteArray& arr)
      {
      arr.reserve(PL.length() + HDR_PDU_LEN);
      arr.append(CMD);
      arr.append((char*)TS, 4);
      arr.append(PL);
      }
   bl_types_t  TYPE;
   int         ARLEN;
   char        CMD;
   clock_t     TS;
   /// @brief "Полезная нагрузка"
   QByteArray  PL;
   /**  --------------------------------------------------------------------------------
     * @brief Преобразование в массив значений в qreal из любых типов
     * @param dst
     * @return - флаг ошибки
     */
   bool toReal(qreal* dst);

   };

#define MAX_PDU_LEN     1024


class CBLiteDecode : public QObject
   {
   Q_OBJECT
public:
   uint32_t pkt_count;
   static QByteArray Write(char cmd, QByteArray& buf, int len)
      {
//      qDebug() << "BL" << QThread::currentThreadId();

      QByteArray iobuf;
      iobuf.clear();
      uint16_t crc = CRC16_INIT;

      // Сигнатура
      iobuf.append("LP", 2);

      // Длина PDU
      uint16_t pdu_len = len + 1;

      iobuf.append((char*)(&pdu_len), 2);
      crc = crc16_update_blk(crc, (uint8_t*)(&pdu_len), 2);

      iobuf.append(cmd);
      // идентификатор пакета
      crc = crc16_update_blk(crc, (uint8_t*)(&cmd), 1);

      // тело пакета
      iobuf.append( buf );

      // контрольная сумма
      crc = crc16_update_blk(crc, (uint8_t*)buf.data(), len);

//      qDebug() << crc;

      iobuf.append((char*)&crc, 2);

      return iobuf;
      }

   bool pkt_parser()
      {
      static uint16_t cnt = 0;
      static uint16_t crc;
      static union
      {
         uint16_t u16val;
         uint8_t  raw[2];
         };
      enum{ CUR = 1, PREV = 0 };
      static fsm_rx_pkt_t fsm_state = FSM_SIGN;
      //        qDebug(data.toHex());
      for(int idx = 0 ; idx < data.length() ;)
         {
         if (fsm_state != FSM_DATA)
            {
            raw[CUR] = data[idx++];
            if (cnt) --cnt;
            }
         // МАШИНА СОСТОЯНИЙ
         switch (fsm_state)
            {
            // Ожидаем приема сигнатуры
            case FSM_SIGN:
               if (raw[PREV] != 'L' || raw[CUR] != 'P') break;
               fsm_state = FSM_LEN;
               cnt = 2;
               break;

               // Принимаем длину пакета
            case FSM_LEN:
               if (cnt) break;

               //                qDebug() << "S";
               if (u16val > MAX_PDU_LEN)
                  {
                  fsm_state = FSM_SIGN;
                  qDebug() << " error length";
                  break;
                  }
               crc = crc16_update_poly(CRC16_INIT, raw[PREV]);
               crc = crc16_update_poly(crc, raw[CUR]);
               packet.clear();
               cnt = u16val;
               fsm_state = FSM_DATA;
               //                qDebug() << "L:" << u16val;
               break;

               // Принимаем пакет данных
            case FSM_DATA:
               if (cnt)
                  {
                  uint8_t x = data[idx];
                  packet += x;
                  crc = crc16_update_blk(crc, &x , 1);
                  idx++; cnt--;
                  }
               if (cnt <= 0)
                  {
                  cnt = 2;
                  fsm_state = FSM_CRC;
                  }
               break;

               // Принимаем и проверяем CRC
            case FSM_CRC:
               if (cnt) break;
               if (u16val != crc)
                  {
                  fsm_state = FSM_SIGN;
                  emit pkterr(packet);
                  break;
                  }
               fsm_state = FSM_VALID;

               // Пакет принят
            case FSM_VALID:
               emit outdata(packet);

            default:
               fsm_state = FSM_SIGN;
               break;
            }

         raw[PREV] = raw[CUR];
         }

      return false;
      }


signals:
   void outdata(QByteArray);
   void pkterr(QByteArray&);

public slots:
   void slotRead();
   void slotData(QByteArray arr)
      {
      data = arr;
      pkt_parser();
      }

private:
   QByteArray  data, packet;

   };



#endif // QBLITEPROTO_H
