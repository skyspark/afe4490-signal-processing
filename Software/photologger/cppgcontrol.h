/**
 * @file cppgcontrol.h
 *
 *
 */

#ifndef CPPGCONTROL_H
#define CPPGCONTROL_H

#include <string.h>
#include <QPair>
#include <QDebug>

#include "ppgproperties.h"
#include "configurator/confparser.h"

class CPPGControl
   {
public:
   afe_regs Regs;

   CPPGControl();
   static const char* namesReg[COUNT_AFE_REG];

   void set_timing(QList<QPair<BitAdress,int> > &list, int duty, int fHZ);
   int setControl(int _nothing);
   void gettiming();
   void setRXStage();
   void get_rxstage(int _nothing);
   int getReg(uint32_t *buf, uint16_t off = 0, uint16_t len = COUNT_AFE_REG);
   void setReg(uint32_t *buf, uint16_t off = 0, uint16_t len = COUNT_AFE_REG);

   ~CPPGControl();

private:

   void setLEDSAMPLE(QList<QPair<BitAdress,int> > &list, uint8_t led, uint16_t cell, uint16_t delay, uint8_t duty = 100);
   void setLEDCONV(QList<QPair<BitAdress, int> > &list, uint8_t led, uint16_t cell, uint16_t gap);
   void setRESET(QList<QPair<BitAdress,int> > &list, uint16_t cell, uint16_t gap);
   };

#endif // CPPGCONTROL_H
