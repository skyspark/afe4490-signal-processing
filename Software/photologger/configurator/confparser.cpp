﻿#include "confparser.h"

ConfParser::ConfParser(QString filePath, LinkHash &_linkHash)
   {
   this->setPath(filePath);
   linkHash = &_linkHash;
   initializeParsing();
   }

void ConfParser::setPath(QString fileName)
   {
   filePath.clear();
   filePath.append(fileName);
   }

void ConfParser::initializeParsing()
   {
   if (!filePath.isEmpty())
      {
      QFile file(filePath);
      if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
         return;

      QTextStream in(&file);
      while (!in.atEnd()) {
         QString line = in.readLine();
         startParsing(line);
         }
      }
   }

void ConfParser::insertToHash() {
   linkHash->insert(bitAdress,bitFieldName,bitProperties);
   }

void ConfParser::clearHashPackage(bool fullClear)
   {
   if (fullClear){
      bitFieldName.clear();
      bitAdress.clear();
      bitProperties.clear();
      }
   else {
      bitFieldName.clear();
      bitAdress.clearTemp();
      bitProperties.clearTemp();
      }
   }

void ConfParser::startParsing(QString line)
   {
   if (line.contains('#')) return;
   if (line.startsWith("RegLen:")) {
      linkHash->regLen = line.split(QRegExp("RegLen:|;")).at(1).toInt();
      return;
      }
   line.replace(" ", "");
   QStringList a = line.split('|');

   readInitRegName(a.at(0));
   readAtributes(a.at(1));
   readAdress(a.at(2));
   readRegLine(a.at(3));
   clearHashPackage(true);
   }

void ConfParser::readInitRegName(QString str) { initRegNames.append(str); }

void ConfParser::readAtributes(QString str) {
   bitProperties.atributes = (str.contains('W')<<1) + str.contains('R');
   }

void ConfParser::readAdress(QString str)
   {
   str.remove(0,2);
   bitAdress.initialize(str.toUInt(0,16));
   }

void ConfParser::readRegLine(QString str)
   {
   QStringList a = str.split(QRegExp(",|;"), QString::SkipEmptyParts);
   foreach (auto i, a) {
      readSingleRegister(i);
      insertToHash();
      clearHashPackage(false);
      }
   }

void ConfParser::readSingleRegister(QString str)
   {
   QStringList a = str.split(QRegExp("(\\[|\\])"), QString::SkipEmptyParts);
   bitFieldName = a.at(0);
   readBitScope(a.at(1));
   if(a.length() > 2)
      readSpecialMethod(a.at(2));
   }

void ConfParser::readBitScope(QString str)
   {
   QStringList a = str.split(':');
   bitAdress.initBit = a.at(0).toUInt();
   bitAdress.lastBit = a.at(1).toUInt();
   }

void ConfParser::readSpecialMethod(QString str) {
   bitProperties.specialMethodName.append(str);
   }

QDebug operator<<(QDebug dbg, const BitAdress &ba)
   {
   QString a = QString("%1[%2:%3]").arg(ba.index).arg(ba.initBit).arg(ba.lastBit);
   dbg << a;
   return dbg.space();
   }
