#ifndef BIHASH_H
#define BIHASH_H

#include <QHash>

template <class T1, class T2, class TProp>

class BiHash
   {

   QHash<T1, T2> forwardHash;
   QHash<T2, T1> reverseHash;
   QHash<T2, TProp> props;

public:
   BiHash():forwardHash(),reverseHash(),props(){ /*Q_ASSERT(typeid(T1)==typeid(T2));*/ }

   void insert(T1 val1, T2 val2, TProp properties)
      {
      forwardHash.insert(val1, val2);
      reverseHash.insert(val2,val1);
      props.insert(val2,properties);
      }

   inline const T1 value(T2 key2) const { return reverseHash.value(key2); }
   inline const T2 value(T1 key1) const { return forwardHash.value(key1); }
   inline const TProp properties(T1 key1) const { return props.value(forwardHash.value(key1)); }
   inline const TProp properties(T2 key2) const { return props.value(key2); }

   QList<T1> keys1() { return forwardHash.keys(); }
   QList<T2> keys2() { return reverseHash.keys(); }

   int remove(T1 key1)
      {
      int i=0;
      T2 key2 = value(key1);
      i += props.remove(key2);
      i += reverseHash.remove(key2);
      i += forwardHash.remove(key1);
      return i;
      }

   int remove(T2 key2)
      {
      int i=0;
      T1 key1 = value(key2);
      i += props.remove(key2);
      i += forwardHash.remove(key1);
      i += reverseHash.remove(key2);
      return i;
      }

   };

#endif // BIHASH_H
