#include <QDebug>
#include "linkergui.h"

#define CALL_MEMBER_FN(object,ptrToMember)  ((object).*(ptrToMember))


void LinkerGUI::init(QString adress) { cp = new ConfParser(adress, linkHash); }

void LinkerGUI::startFunc()
   {
   for(int i = 0; i<widgets.length(); ++i)
      {
      QCheckBox *checkBox = qobject_cast<QCheckBox*>(widgets.at(i));
      QSpinBox   *spinBox = qobject_cast <QSpinBox*>(widgets.at(i));
      QComboBox *comboBox = qobject_cast<QComboBox*>(widgets.at(i));

      if (!(checkBox || spinBox || comboBox))
         continue;

      QString regName = objNameToStr(widgets.at(i)->objectName());
      if ( !linkHash.value(regName) && !functionExists(regName))
         continue;

      if (checkBox)
         takeCheckBox(checkBox);
      else if (spinBox)
         takeSpinBox(spinBox);
      else if (comboBox)
         takeComboBox(comboBox);
      }
   }

QString LinkerGUI::objNameToStr(QString objName)
   {
   while (objName.at(0).isLower())
      objName.remove(0,1);
   return objName;
   }

void LinkerGUI::takeCheckBox(QCheckBox *checkBox)
   {
   connect(checkBox, &QCheckBox::toggled, this, [=](const int &newValue) {
      writeValueByRegName(checkBox->objectName(), newValue);
      });
   }

void LinkerGUI::takeSpinBox(QSpinBox *spinBox)
   {
   connect(spinBox, (void(QSpinBox::*)(int))&QSpinBox::valueChanged, this, [=](const int &newValue) {
      writeValueByRegName(spinBox->objectName(), newValue);
      });
   }

void LinkerGUI::takeComboBox(QComboBox *comboBox)
   {
   connect(comboBox, (void(QComboBox::*)(int))&QComboBox::currentIndexChanged, this, [=](const int &newValue) {
      writeValueByRegName(comboBox->objectName(), newValue);
      });
   }

void LinkerGUI::writeRegister(BitAdress adr, int value, bool shouldEmit)
   {
   uint32_t len = adr.lastBit-adr.initBit+1;
   uint32_t mask = (int)pow(2,len)-1;
   this->regs->raw[adr.index] &= ~(mask<<adr.initBit);
   this->regs->raw[adr.index] |= (value<<adr.initBit);
   emit regsChanged(adr);
   if (shouldEmit) {
      emit regsChanged();
      }
   }

void LinkerGUI::writeValueByRegName(const QString objectName, int value , bool shouldWriteRaw)
   {
   QString regName = objNameToStr(objectName);
   BitProperties bitProp = linkHash.properties(regName);

   BitAdress bitAdr = linkHash.value(regName);

   if (functionExists(regName) && !shouldWriteRaw){
      auto a = specFunctionsHash.value(regName);
      if (a != NULL){
         value = CALL_MEMBER_FN(*window,a)(value);
         }

      auto b = specArrayFunctionsHash.value(regName);
      if (b != NULL){
         for(auto c : CALL_MEMBER_FN(*window,b)(value))
            writeRegister(c.first, c.second, false);
         emit regsChanged();
         return;
         }
      }
   writeRegister(bitAdr, value);
   return;
   }

void LinkerGUI::appendSpecialMethodsHash(QString regName, intFunc_t func) {
   this->specFunctionsHash.insert(regName, func);
   }

bool LinkerGUI::functionExists(QString str)
   { bool a;
     a = (bool)specArrayFunctionsHash.value(str)
         || (bool)specFunctionsHash.value(str);
       return a;
      }

void LinkerGUI::appendSpecialMethodsHash(QString regName, arrayFunc_t func) {
   this->specArrayFunctionsHash.insert(regName, func);
   }

void LinkerGUI::printFields(bool isUsed)
   {
   //   foreach(auto a, this->linkHash.keys1()){
   //      if (linkHash.properties(a).connected == isUsed)
   //         //qDebug() << QString("RegName: %1").arg(a)
   //         ;
   //      }
   }

