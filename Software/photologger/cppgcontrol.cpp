#include "cppgcontrol.h"


CPPGControl::CPPGControl()
   {
   for( int i=0; i<COUNT_AFE_REG; ++i){
      Regs.raw[i]=0;
      }

   }


void CPPGControl::set_timing(QList<QPair<BitAdress, int> > &list, int duty, int fHZ)
   {
   if (fHZ > MAX_SAMPLE_FREQ_HZ) fHZ = MAX_SAMPLE_FREQ_HZ;
   // Прескалер
   uint32_t PRP  = CLK_FREQ_HZ / fHZ;
   uint32_t cell = PRP  / 4;

   // ЗАХВАТ УВХ


   setLEDSAMPLE(list, 1, cell, CONV_START_DELAY, duty);
   setLEDSAMPLE(list, 2, cell, CONV_START_DELAY, duty);

   // ПРЕОБРАЗОВАНИЯ АЦП
   setLEDCONV  (list, 1, cell, CONV_TIME_GAP);
   setLEDCONV  (list, 2, cell, CONV_TIME_GAP);

   // Сброс рeгистров AЦП
   setRESET(list, cell, CONV_TIME_GAP);

   BitAdress ba(PRPCOUNT,15,0);
   list.append(QPair<BitAdress,int>(ba, PRP-1));
   return;
   }

int CPPGControl::setControl(int _nothing)
   {
   ///TODO: на выброс??
   //      uint32_t Nav = (5000 / sampleFreqHz) - 1;
   //      if (ppg_ctrl.aver > Nav)  ppg_ctrl.aver = Nav;
   return 0;
   }

///WARNING: refactoring needed
void CPPGControl::setRXStage()
   {
   // -----------------------------------------------------
   // Cf -> code
   uint8_t qCf[] = { 0, 5, 15, 25, 50, 150 };

   auto calcCfCode = [=](uint8_t c)-> uint8_t {
      uint8_t res = 0;
      for(uint i=0; i<sizeof(qCf); i++)
         {
         if (c&1) res+=qCf[i];
         c >>= 1;
         }

      return res;
      };
   // -----------------------------------------------------
   // code -> Cf
   auto calcCodeCf = [=](uint8_t c)-> uint8_t {
      uint8_t res=0;
      for(uint i=0 ;i<sizeof(qCf)-1; i++)
         {
         res <<= 1;
         uint8_t cref = qCf[sizeof(qCf) - i - 1];
         if (c>cref){ res |= 1; c -= cref; }
         }
      return res;
      };


   uint8_t tmpCf[2] = {0, 0};
   tmpCf[0] = calcCodeCf(Regs.tiagain.cf_led);
   tmpCf[1] = calcCodeCf(Regs.tiaambgain.cf_led);
   // Максимальный ток 10uA шаг 1uA
   Regs.tiaambgain.AMBDAC  %= 10;

   Regs.tiagain.cf_led = calcCfCode(tmpCf[0]);
   Regs.tiaambgain.cf_led = calcCfCode(tmpCf[1]);

   }

///WARNING: refactoring needed
void CPPGControl::gettiming()
   {
   int duty = 100;
   //   int fHz = Reg[PRPCOUNT]*CLK_FREQ_HZ;
   }

void CPPGControl::get_rxstage(int _nothing)
   {
   // -----------------------------------------------------
   // Cf -> code
   uint8_t qCf[] = { 0, 5, 15, 25, 50, 150 };

   auto calcCfCode = [=](uint8_t c)->uint8_t {
      uint8_t res = 0;

      for(uint i=0; i<sizeof(qCf); i++) {
         if (c&1) res+=qCf[i];
         c >>= 1;
         }
      return res;
      };
   }

int CPPGControl::getReg(uint32_t *buf, uint16_t off, uint16_t len)
   {
   //INFO: что это?
   int count = len;
   if (count > MAX_AFE_REG_NUM) count = MAX_AFE_REG_NUM;

   //Reg[SPARE1] = 0;

   //   for(int i = SPARE2; i<COUNT_AFE_REG; i++) Reg[i] = 0;
   //   memcpy(buf, &Reg[off], count*4);

   return count;
   }

void CPPGControl::setReg(uint32_t *buf, uint16_t off, uint16_t len)
   {
   //INFO: что это?
   int count = len;
   if (count < MAX_AFE_REG_NUM) count = MAX_AFE_REG_NUM;
   //   memcpy(&Reg [off], buf, count * 4);
   }

CPPGControl::~CPPGControl()
   {
   }

void CPPGControl::setLEDSAMPLE(QList<QPair<BitAdress,int> > &list, uint8_t led, uint16_t cell, uint16_t delay, uint8_t duty)
   {
   uint8_t off = (led == 1)?(LED1STC) : (LED2STC);
   uint8_t n  = 2 * (led - 1) + 1;

   uint16_t x = n * cell;
   uint16_t k = 100 / duty;
   uint16_t y = cell / k;
   BitAdress Adress(off-1,15,0);
   list.append(QPair<BitAdress,int>(Adress.indexInc(), x + delay));
   list.append(QPair<BitAdress,int>(Adress.indexInc(), x + y - 2));
   list.append(QPair<BitAdress,int>(Adress.indexInc(), x));
   list.append(QPair<BitAdress,int>(Adress.indexInc(), x + y - 1));

   n = (n > 2) ? 0 : (n + 1);
   x = n * cell;

   list.append(QPair<BitAdress,int>(Adress.indexInc(), x + delay));
   list.append(QPair<BitAdress,int>(Adress.indexInc(), x + y - 2));
   }


void CPPGControl::setLEDCONV(QList<QPair<BitAdress,int> > &list, uint8_t led, uint16_t cell, uint16_t gap)
   {
   uint8_t off = (led == 1)?(LED1CONVST) : (LED2CONVST);
   uint8_t n  =  (led == 1)?(2) : (0);

   BitAdress Adress(off-1,15,0);

   list.append(QPair<BitAdress,int>(Adress.indexInc(), n * cell + gap));  n++;
   list.append(QPair<BitAdress,int>(Adress.indexInc(), n * cell - 1));
   list.append(QPair<BitAdress,int>(Adress.indexInc(), n * cell + gap));  n++;
   list.append(QPair<BitAdress,int>(Adress.indexInc(), n * cell - 1));
   }

void CPPGControl::setRESET(QList<QPair<BitAdress, int> > &list, uint16_t cell, uint16_t gap)
   {
   uint8_t off = ADCRSTCNT0;
   BitAdress Adress(off-1,15,0);

   for(int i = 0 ; i < 4 ; i++)
      {
      list.append(QPair<BitAdress,int>(Adress.indexInc(), i * cell));
      list.append(QPair<BitAdress,int>(Adress.indexInc(), i * cell + gap - 1));
      }
   }

