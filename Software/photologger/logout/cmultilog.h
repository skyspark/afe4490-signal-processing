/**
 * Вывод лога в любое текстовое поле
 */

#ifndef CMULTILOG_H
#define CMULTILOG_H

#include <QtGui>
#include <QWidget>
#include <QTextEdit>
#include <QDebug>
#include <QtWidgets>
/**
 * Выводим в лог через это определение
 */
#define Log  CLog :: log()

/**
 * @brief The clear class Символьный класс очистки лога
 */
class clear{};


/**
 * @brief The CMultiLog class Вывод логов в текстовом поле
 */
class CMultiLog : public QWidget
{
    Q_OBJECT
private:
    /**
     * @brief pte Поле вывода текстовой информации
     */
public:
    QTextEdit* pte;
    explicit CMultiLog(QTextEdit* pte, QWidget *parent = 0);

    /**
     * @brief prn - распечатка строки
     * @param str - строка для вывода
     * @return - ссылка на себя
     */
    void prn(QString str);

    /**
     * @brief clr Очистка лога
     */
    inline void clr()
    {
        emit pte->clear();
    }
    /**
     * @brief color Установка цвета шрифта
     * @param c цвет
     */
    inline void color(Qt::GlobalColor c)
    {
        pte->setTextColor(c);
    }
    /**
     * @brief fontweight
     * @param w
     */
    inline void fontweight(QFont :: Weight w)
    {
        pte->setFontWeight(w);
    }

signals:
    /**
     * @brief sigMsg Сигнал для передачи строки
     * @param str
     */
    void sigMsg(const QString& str);

public slots:

   /**
    * @brief saveMsg Сохранение в файл
    * @param fn
    */
    void saveMsg(const QString& fn);

    /**
     * @brief prnMsg - распечатывает строку str
     * @param str - строка
     */
    void prnMsg(const QString& str);
};

/*****************************************************************/
/*****************************************************************/

/**
 * @brief The CLog class Обертка для СMultiLog
 *
 */
class CLog : public QWidget
{
    Q_OBJECT
private:
    friend class CMultiLog;
    /**
     * @brief operator << Очистка
     * @param log - ссылка на мультилог
     * @return ссылка на мультилог
     */
    friend CMultiLog& operator << (CMultiLog& log, clear);

    /**
     * @brief operator << Шаблон различных выводов
     * @param log - ссылка на мультилог
     * @return ссылка на мультилог
     */
    template< class T > friend
    CMultiLog& operator << (CMultiLog& log, T str);

    /**
     * @brief operator << Смена цвета
     * @param log - мультилог
     * @param c - цвет
     * @return ссылка на мультилог
     */
    friend CMultiLog& operator << (CMultiLog& log, Qt::GlobalColor c);

    friend CMultiLog& operator << (CMultiLog& log, QFont :: Weight w);

    friend CMultiLog& operator << (CMultiLog& log, QByteArray& arr);

    /**
      * @brief pml - указатель на мультилог
      */
     static QPointer<CMultiLog> pml;

     /**
      * @brief build Инициализация
      */
     static QWidget* build(QWidget* parent)
     {
         if (!pml.isNull())  // Второй раз не создаем
             return pml->pte;
         QTextEdit* pte = new QTextEdit(parent);
         pml = new CMultiLog(pte, parent);
         pte->document()->setMaximumBlockCount(100);
         QObject :: connect(pml, SIGNAL(sigMsg(const QString&)), pml, SLOT(prnMsg(const QString&)));
         return pml->pte;
     }

public:
     /**
      * @brief CLog Конструктор
      * @param pte - указатель на текстовое поле
      * @param parent - указатель на родителя
      */
     CLog(QWidget* parent)
     {
        build(parent);
        if (this->objectName().isEmpty()) this->setObjectName(QStringLiteral("Log"));
        this->resize(0, 270);
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::MinimumExpanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        QGridLayout* gridLayout = new QGridLayout(this);
        gridLayout->addWidget(build(this));
     }

     /**
      * @brief log Возвращает ссылку на мультилог
      * @return
      */
     static CMultiLog& log(){ return *pml; }

     /**
      * @brief savetofile Сохранение в файл
      * @param fn - имя файла
      */
     static void savetofile(const QString& fn) { emit pml->saveMsg(fn);  }
};

/*****************************************************************/
/*****************************************************************/

template< class T >
inline CMultiLog& operator << (CMultiLog& log, T str)
{
    if (CLog :: pml)
    {
        QVariant tmp(str);
        log.prn(tmp.toString());
    }
    return log;
}

inline CMultiLog& operator << (CMultiLog& log, clear)
{
    if (CLog :: pml)
        log.clr();
    return log;
}

inline CMultiLog& operator << (CMultiLog& log, Qt :: GlobalColor c)
{
    if (CLog :: pml)
        log.color(c);
    return log;
}

inline CMultiLog& operator << (CMultiLog& log, QFont :: Weight w)
{
    if (CLog :: pml)
        log.fontweight(w);
    return log;
}

inline CMultiLog& operator << (CMultiLog& log, QByteArray& arr)
{
    QString str, s;
    for(int i = 0; i < arr.count() ; i++)
    {
        if (((i % 16) == 0) && (i != 0))
        {
            log << str;
            str = "";
        }
        str += s.setNum((uchar)arr[i], 16).prepend(" 0x");
    }
    log << str;
    return log;
}

/**
 * Шаблон класса вывода цветных строк
 */
template < int color >
class CLogColorStr
{
private:
    QString str;
public:
    enum{ COLOR = color };
    CLogColorStr(QString s) : str(s) {}
    QString& get_str() { return str; }
};

/**
 * Макрос сборки классов цветных строк и
 * функций вывода в лог
 */
#define COLORSTR(name, c)                                 \
class name : public CLogColorStr< c > {                      \
public: name(QString s) : CLogColorStr< c >(s) {} };     \
                                                             \
inline CMultiLog& operator << (CMultiLog& log, name cc)   \
{   log.color(Qt :: GlobalColor (name :: COLOR));        \
    log.prn(cc.get_str());                                 \
    log.color(Qt :: black);                                \
    return log; }


/**
 * Классы и функции вывода цветных строк
 */
COLORSTR(good   , Qt :: darkGreen)
COLORSTR(error  , Qt :: darkRed  )
COLORSTR(warning, Qt :: darkYellow)
COLORSTR(info   , Qt :: darkBlue )


#endif // CMULTILOG_H
