/**
 * Вывод лога в любое текстовое поле
 */
#include "cmultilog.h"

QPointer<CMultiLog> CLog :: pml = NULL;

/**
 * Конструктор
 * @brief CMultiLog::CMultiLog
 * @param parent
 */
CMultiLog::CMultiLog(QTextEdit* pTextEdit, QWidget *parent) :
    QWidget(parent),pte(pTextEdit)
{
    pte->setReadOnly(true);
}

/**
 * @brief prn - распечатка строки
 * @param str - строка для вывода
 * @return - указатель на себя
 */
void CMultiLog::prn(QString str)
{
    emit sigMsg(str);
}

/**
 * @brief prnMsg - распечатывает строку str
 * @param str - строка
 */
void CMultiLog :: prnMsg(const QString& str)
{
    if (!pte)
        return;
    QTime Tm(0,0);
    QString str_time = Tm.currentTime().toString("hh:mm:ss.zzz");
    pte->append(str_time + "  " + str);
}

/**
 * @brief saveMsg Сохранение в файл
 * @param fn
 */
void  CMultiLog :: saveMsg(const QString& fn)
{
    QFile outfile;
    outfile.setFileName(fn);
    outfile.open(QIODevice :: Append | QIODevice :: Text);
    if (outfile.error())
        return;

    QTextStream  out(&outfile);
    out << pte->toPlainText();

    outfile.close();
}

