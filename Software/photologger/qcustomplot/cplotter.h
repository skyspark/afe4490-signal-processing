#ifndef CPLOTTER_H
#define CPLOTTER_H

#include <QWidget>
#include <QTime>
#include "qcustomplot.h"

namespace Ui {
class cplotter;
}

class cplotter : public QWidget
{
    Q_OBJECT

public:
    explicit cplotter(QWidget *parent = 0);
    ~cplotter();
public slots:
    void addPoint(const QByteArray &arr);

    void close(void){  }

    void closeEvent(QCloseEvent);

private slots:
    /**
     * @brief Слот перерисовки графика
     */
    void onTimer();
    /**
     * @brief titleDoubleClick
     * @param event
     * @param title
     */

    void titleDoubleClick(QMouseEvent *event, QCPPlotTitle *title);
    /**
     * @brief axisLabelDoubleClick
     * @param axis
     * @param part
     */
    void axisLabelDoubleClick(QCPAxis* axis, QCPAxis::SelectablePart part);
    /**
     * @brief legendDoubleClick
     * @param legend
     * @param item
     */
    void legendDoubleClick(QCPLegend* legend, QCPAbstractLegendItem* item);
    /**
     * @brief selectionChanged
     */
    void selectionChanged();
    /**
     * @brief mousePress
     */
    void mousePress();
    /**
     * @brief mouseWheel
     */
    void mouseWheel();


    void setInterval();
    /**
     * @brief removeSelectedGraph
     */
    void removeSelectedGraph();
    /**
     * @brief removeAllGraphs
     */
    void removeAllGraphs();
    /**
     * @brief contextMenuRequest
     * @param pos
     */
    void contextMenuRequest(QPoint pos);
    /**
     * @brief moveLegend
     */
    void moveLegend();
    /**
     * @brief graphClicked
     * @param plottable
     */
    void graphClicked(QCPAbstractPlottable *plottable);

    void onFreqTimer();

private:
    QPen randomPen()
    {
        QPen graphPen;
        graphPen.setColor(QColor(rand() % 245 + 10, rand() % 245 + 10, rand() % 245 + 10));
        graphPen.setWidthF(rand()/(double)RAND_MAX * 2 + 1);
        return graphPen;
    }

    int replotInterval;

    /** ---------------------------------------------------------------------
     * @brief ui
     */
    QTimer*     pTimer;

    QTimer* freqTimer;
    QTime     freqTime;
    QTime     debugTimer;
    qreal       prevVal;
    int peak,peak2 ;
    bool alreadyUsed;
    int pulseTick;


    int         plotLen;
    int         graphNum;
    uint32_t    tick;
    /** ---------------------------------------------------------------------
     * @brief Указатель на менеджер данных
     */

private:
    Ui::cplotter *ui;
};

#endif // CPLOTTER_H
