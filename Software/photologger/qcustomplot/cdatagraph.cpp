#include "cdatagraph.h"
#include <QMenu>
#include <qcustomplot.h>

CWdtMPU::CWdtMPU( QWidget *parent) :
    QWidget(parent), replotInterval( 100 ) , plotLen( 2000 ) , graphNum( 6 ) , ui(new Ui::CWdtMPU)
{



    pCPlot->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom | QCP::iSelectAxes |
                                    QCP::iSelectLegend | QCP::iSelectPlottables);


    pCPlot->xAxis->setLabel("time");
    pCPlot->yAxis->setLabel("y Axis");
    pCPlot->legend->setVisible(true);
    QFont legendFont = font();
    legendFont.setPointSize( 8 );
    pCPlot->legend->setFont(legendFont);
    pCPlot->legend->setSelectedFont(legendFont);
    pCPlot->legend->setSelectableParts(QCPLegend::spItems); // legend box shall not be selectable, only legend items
    pCPlot->setAntialiasedElement( QCP::aeNone , true );

    // connect slot that ties some axis selections together (especially opposite axes):
    connect(pCPlot, SIGNAL(selectionChangedByUser()), this, SLOT(selectionChanged()));

    // connect slots that takes care that when an axis is selected, only that direction can be dragged and zoomed:
    connect(pCPlot, SIGNAL(mousePress(QMouseEvent*)), this, SLOT(mousePress()));
    connect(pCPlot, SIGNAL(mouseWheel(QWheelEvent*)), this, SLOT(mouseWheel()));

    // make bottom and left axes transfer their ranges to top and right axes:
    connect(pCPlot->xAxis, SIGNAL(rangeChanged(QCPRange)), pCPlot->xAxis2, SLOT(setRange(QCPRange)));
    connect(pCPlot->yAxis, SIGNAL(rangeChanged(QCPRange)), pCPlot->yAxis2, SLOT(setRange(QCPRange)));

    // connect some interaction slots:
    connect(pCPlot, SIGNAL(titleDoubleClick(QMouseEvent*,QCPPlotTitle*)), this, SLOT(titleDoubleClick(QMouseEvent*,QCPPlotTitle*)));
    connect(pCPlot, SIGNAL(axisDoubleClick(QCPAxis*,QCPAxis::SelectablePart,QMouseEvent*)), this, SLOT(axisLabelDoubleClick(QCPAxis*,QCPAxis::SelectablePart)));
    connect(pCPlot, SIGNAL(legendDoubleClick(QCPLegend*,QCPAbstractLegendItem*,QMouseEvent*)), this, SLOT(legendDoubleClick(QCPLegend*,QCPAbstractLegendItem*)));

    // connect slot that shows a message in the status bar when a graph is clicked:
    connect(pCPlot, SIGNAL(plottableClick(QCPAbstractPlottable*,QMouseEvent*)), this, SLOT(graphClicked(QCPAbstractPlottable*)));

    // setup policy and connect slot for context menu popup:
    pCPlot->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(pCPlot, SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(contextMenuRequest(QPoint)));

   pCPlot->setNoAntialiasingOnDrag( true );

    pCPlot->yAxis->setRange( 0 , (qreal)(  1.0 ) );
    QStringList strlist;
    strlist << "Red Data" << "Red Ambient Data" << "IR Data" << "IR Ambient"  << "Red Different" << "IR Different";
    for( int i = 0 ; i < graphNum ; i++ )
    {
        pCPlot->addGraph( );
        pCPlot->graph( )->setName( strlist[ i ] );
        pCPlot->graph( )->setPen( randomPen( ) );
        pCPlot->graph( )->setAdaptiveSampling( true );
    }
    pCPlot->axisRect()->setupFullAxesBox();

}

CWdtMPU::~CWdtMPU()
{
   // pTimer->stop( );
}

void CWdtMPU::addPoint(const QByteArray &arr)
{

    static uint tick = 0, points = 0;
    ++tick;
    ++points;
     for( int i = 0 ; i < graphNum ; i++ )
     {
          qreal val =  ( ( qreal ) arr[ i ] / 16777216.0 );
          pCPlot->graph( i )->addData( ( qreal )tick , val );
          pCPlot->graph( i )->removeData(tick - plotLen);

     }

     if( points % replotInterval == 0 )
     {
         pCPlot->xAxis->setRange( tick+10 , plotLen , Qt::AlignRight );
         pCPlot->replot( );
     }
}


void CWdtMPU :: titleDoubleClick(QMouseEvent* event, QCPPlotTitle* title)
{
  Q_UNUSED(event)
  // Set the plot title by double clicking on it
  bool ok;
  QString newTitle = QInputDialog::getText(this, "QCustomPlot example", "New plot title:", QLineEdit::Normal, title->text(), &ok);
  if (ok)
  {
    title->setText( newTitle );
    pCPlot->replot( );
  }
}

void CWdtMPU :: axisLabelDoubleClick(QCPAxis *axis, QCPAxis::SelectablePart part)
{
  // Set an axis label by double clicking on it
  if (part == QCPAxis::spAxisLabel) // only react when the actual axis label is clicked, not tick label or axis backbone
  {
    bool ok;
    QString newLabel = QInputDialog::getText(this, "QCustomPlot example", "New axis label:", QLineEdit::Normal, axis->label(), &ok);
    if (ok)
    {
      axis->setLabel(newLabel);
      pCPlot->replot();
    }
  }
}

void CWdtMPU :: legendDoubleClick(QCPLegend *legend, QCPAbstractLegendItem *item)
{
  // Rename a graph by double clicking on its legend item
  Q_UNUSED(legend)
  if (item) // only react if item was clicked (user could have clicked on border padding of legend where there is no item, then item is 0)
  {
    QCPPlottableLegendItem *plItem = qobject_cast<QCPPlottableLegendItem*>(item);
    bool ok;
    QString newName = QInputDialog::getText(this, "QCustomPlot example", "New graph name:", QLineEdit::Normal, plItem->plottable()->name(), &ok);
    if (ok)
    {
      plItem->plottable()->setName(newName);
      pCPlot->replot();
    }
  }
}

void CWdtMPU :: selectionChanged()
{
  /*
   normally, axis base line, axis tick labels and axis labels are selectable separately, but we want
   the user only to be able to select the axis as a whole, so we tie the selected states of the tick labels
   and the axis base line together. However, the axis label shall be selectable individually.

   The selection state of the left and right axes shall be synchronized as well as the state of the
   bottom and top axes.

   Further, we want to synchronize the selection of the graphs with the selection state of the respective
   legend item belonging to that graph. So the user can select a graph by either clicking on the graph itself
   or on its legend item.
  */

  // make top and bottom axes be selected synchronously, and handle axis and tick labels as one selectable object:
  if (pCPlot->xAxis->selectedParts().testFlag(QCPAxis::spAxis) || pCPlot->xAxis->selectedParts().testFlag(QCPAxis::spTickLabels) ||
      pCPlot->xAxis2->selectedParts().testFlag(QCPAxis::spAxis) || pCPlot->xAxis2->selectedParts().testFlag(QCPAxis::spTickLabels))
  {
    pCPlot->xAxis2->setSelectedParts(QCPAxis::spAxis|QCPAxis::spTickLabels);
    pCPlot->xAxis->setSelectedParts(QCPAxis::spAxis|QCPAxis::spTickLabels);
  }
  // make left and right axes be selected synchronously, and handle axis and tick labels as one selectable object:
  if (pCPlot->yAxis->selectedParts().testFlag(QCPAxis::spAxis) || pCPlot->yAxis->selectedParts().testFlag(QCPAxis::spTickLabels) ||
      pCPlot->yAxis2->selectedParts().testFlag(QCPAxis::spAxis) || pCPlot->yAxis2->selectedParts().testFlag(QCPAxis::spTickLabels))
  {
    pCPlot->yAxis2->setSelectedParts(QCPAxis::spAxis|QCPAxis::spTickLabels);
    pCPlot->yAxis->setSelectedParts(QCPAxis::spAxis|QCPAxis::spTickLabels);
  }

  // synchronize selection of graphs with selection of corresponding legend items:
  for (int i=0; i<pCPlot->graphCount(); ++i)
  {
    QCPGraph *graph = pCPlot->graph(i);
    QCPPlottableLegendItem *item = pCPlot->legend->itemWithPlottable(graph);
    if (item->selected() || graph->selected())
    {
      item->setSelected(true);
      graph->setSelected(true);
    }
  }
}

void CWdtMPU :: mousePress()
{
  // if an axis is selected, only allow the direction of that axis to be dragged
  // if no axis is selected, both directions may be dragged

  if (pCPlot->xAxis->selectedParts().testFlag(QCPAxis::spAxis))
    pCPlot->axisRect()->setRangeDrag(pCPlot->xAxis->orientation());
  else if (pCPlot->yAxis->selectedParts().testFlag(QCPAxis::spAxis))
    pCPlot->axisRect()->setRangeDrag(pCPlot->yAxis->orientation());
  else
    pCPlot->axisRect()->setRangeDrag(Qt::Horizontal|Qt::Vertical);
}

void CWdtMPU :: mouseWheel()
{
  // if an axis is selected, only allow the direction of that axis to be zoomed
  // if no axis is selected, both directions may be zoomed

  if ( pCPlot->xAxis->selectedParts( ).testFlag(QCPAxis::spAxis ) )
    pCPlot->axisRect( )->setRangeZoom( pCPlot->xAxis->orientation( ) );
  else if (pCPlot->yAxis->selectedParts().testFlag(QCPAxis::spAxis))
    pCPlot->axisRect()->setRangeZoom(pCPlot->yAxis->orientation());
  else
      pCPlot->axisRect()->setRangeZoom(Qt::Horizontal|Qt::Vertical);
}

///** ----------------------------------------------------------------


/** ----------------------------------------------------------------
 * @brief Слот установки интервала
 */
void CWdtMPU :: setInterval( )
{
    QInputDialog* pqid = new QInputDialog( this );
    replotInterval = pqid->getInt( this , "Replot interval" , "ms" , replotInterval , 10 , 2000 );
//    pTimer->setInterval( replotInterval );
    pCPlot->yAxis->setRange( 0 , (qreal)(  1.0 ) );
}
/** ----------------------------------------------------------------
 * @brief Слот обработки установки положения легенды
 */
void CWdtMPU :: removeSelectedGraph()
{
  if (pCPlot->selectedGraphs().size() > 0)
  {
    //pCPlot->removeGraph(pCPlot->selectedGraphs().first());
    //pCPlot->replot();
  }
}
/** ----------------------------------------------------------------
 * @brief Слот удаления всех графиков
 */
void CWdtMPU :: removeAllGraphs()
{
//    for( int i = 0 ; i < ds_list.length( ) ; i++ )
//        delete ( ds_list[ i ] );
    pCPlot->clearGraphs( );
    pCPlot->replot( );
}
/** ----------------------------------------------------------------
 * @brief Слот обработки установки положения легенды
 */
void CWdtMPU :: contextMenuRequest(QPoint pos)
{
  QMenu *menu = new QMenu(this);
  menu->setAttribute(Qt::WA_DeleteOnClose);

  if (pCPlot->legend->selectTest(pos, false) >= 0) // context menu on legend requested
  {
    menu->addAction("Move to top left", this, SLOT(moveLegend()))->setData((int)(Qt::AlignTop|Qt::AlignLeft));
    menu->addAction("Move to top center", this, SLOT(moveLegend()))->setData((int)(Qt::AlignTop|Qt::AlignHCenter));
    menu->addAction("Move to top right", this, SLOT(moveLegend()))->setData((int)(Qt::AlignTop|Qt::AlignRight));
    menu->addAction("Move to bottom right", this, SLOT(moveLegend()))->setData((int)(Qt::AlignBottom|Qt::AlignRight));
    menu->addAction("Move to bottom left", this, SLOT(moveLegend()))->setData((int)(Qt::AlignBottom|Qt::AlignLeft));
  } else  // general context menu on graphs requested
  {
    //menu->addAction("Add graph", this, SLOT( addGraph( ) ) );
    if (pCPlot->selectedGraphs().size() > 0)
      menu->addAction("Remove selected graph", this, SLOT( removeSelectedGraph( ) ) );
    if (pCPlot->graphCount() > 0)
      menu->addAction("Remove all graphs", this, SLOT( removeAllGraphs( ) ) );
    menu->addAction( "Set replot interval", this, SLOT( setInterval( ) ) );
  }

  menu->popup(pCPlot->mapToGlobal(pos));
}
/** ----------------------------------------------------------------
 * @brief Слот обработки установки положения легенды
 */
void CWdtMPU :: moveLegend()
{
  if (QAction* contextAction = qobject_cast<QAction*>(sender())) // make sure this slot is really called by a context menu action, so it carries the data we need
  {
    bool ok;
    int dataInt = contextAction->data().toInt(&ok);
    if (ok)
    {
      pCPlot->axisRect()->insetLayout()->setInsetAlignment(0, (Qt::Alignment)dataInt);
      pCPlot->replot();
    }
  }
}

/** ----------------------------------------------------------------
 * @brief Слот обработки клика мышки
 */
void CWdtMPU :: graphClicked(QCPAbstractPlottable */*plottable*/)
{
  //ui->statusBar->showMessage(QString("Clicked on graph '%1'.").arg(plottable->name()), 1000);
}

/** ----------------------------------------------------------------
 * @brief Слот перерисовки графика
 */
void CWdtMPU :: onTimer( )
{
//    pCPlot->replot( );
}
