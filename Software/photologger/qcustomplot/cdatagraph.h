#ifndef CDATAGRAPH_H
#define CDATAGRAPH_H

#include <QObject>
#include <qcustomplot.h>
#include <ui_cwdtmpu.h>

namespace Ui {
class CWdtMPU;
}
/** --------------------------------------------------------------------
 *
 *
 * @brief Виджет отрисовки графиков
 *
 *
 */
class CWdtMPU : public QWidget
{
    Q_OBJECT

public:
    explicit CWdtMPU(QWidget *parent = 0);
    ~CWdtMPU();

public slots:
    void addPoint( const QByteArray &arr );

private slots:
    /**
     * @brief Слот перерисовки графика
     */
    void onTimer( );
    /**
     * @brief titleDoubleClick
     * @param event
     * @param title
     */
    void titleDoubleClick(QMouseEvent *event, QCPPlotTitle *title);
    /**
     * @brief axisLabelDoubleClick
     * @param axis
     * @param part
     */
    void axisLabelDoubleClick(QCPAxis* axis, QCPAxis::SelectablePart part);
    /**
     * @brief legendDoubleClick
     * @param legend
     * @param item
     */
    void legendDoubleClick(QCPLegend* legend, QCPAbstractLegendItem* item);
    /**
     * @brief selectionChanged
     */
    void selectionChanged( );
    /**
     * @brief mousePress
     */
    void mousePress( );
    /**
     * @brief mouseWheel
     */
    void mouseWheel( );


    void setInterval( );
    /**
     * @brief removeSelectedGraph
     */
    void removeSelectedGraph( );
    /**
     * @brief removeAllGraphs
     */
    void removeAllGraphs( );
    /**
     * @brief contextMenuRequest
     * @param pos
     */
    void contextMenuRequest(QPoint pos);
    /**
     * @brief moveLegend
     */
    void moveLegend();
    /**
     * @brief graphClicked
     * @param plottable
     */
    void graphClicked( QCPAbstractPlottable *plottable );
private:
    QPen randomPen(  )
    {
        QPen graphPen;
        graphPen.setColor( QColor( rand( ) % 245 + 10, rand( ) % 245 + 10, rand( ) % 245 + 10 ) );
        graphPen.setWidthF( rand( )/( double )RAND_MAX * 2 + 1 );
        return graphPen;
    }

    int replotInterval;

    /** ---------------------------------------------------------------------
     * @brief ui
     */
    QTimer* pTimer;
    int plotLen;
    int graphNum;
    Ui::CWdtMPU *ui;
    /** ---------------------------------------------------------------------
     * @brief Указатель на менеджер данных
     */
};

#endif // CDATAGRAPH_H
