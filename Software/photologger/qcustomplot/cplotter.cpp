﻿#include "cplotter.h"
#include "ui_cplotter.h"
#include "ppgproperties.h"

cplotter::cplotter(QWidget *parent) :
   QWidget(parent), replotInterval(100), plotLen(1000), graphNum(6),
   ui(new Ui::cplotter)
   {
   ui->setupUi(this);



   ui->widget->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom | QCP::iSelectAxes |
                               QCP::iSelectLegend | QCP::iSelectPlottables);


   ui->widget->xAxis->setLabel("time");
   ui->widget->yAxis->setLabel("y Axis");
   ui->widget->legend->setVisible(true);
   QFont legendFont = font();
   legendFont.setPointSize(8);
   ui->widget->legend->setFont(legendFont);
   ui->widget->legend->setSelectedFont(legendFont);
   ui->widget->legend->setSelectableParts(QCPLegend::spItems); // legend box shall not be selectable, only legend items
   ui->widget->setAntialiasedElement(QCP::aeNone, true);

   // connect slot that ties some axis selections together (especially opposite axes):
   connect(ui->widget, SIGNAL(selectionChangedByUser()), this, SLOT(selectionChanged()));

   // connect slots that takes care that when an axis is selected, only that direction can be dragged and zoomed:
   connect(ui->widget, SIGNAL(mousePress(QMouseEvent*)), this, SLOT(mousePress()));
   connect(ui->widget, SIGNAL(mouseWheel(QWheelEvent*)), this, SLOT(mouseWheel()));

   // make bottom and left axes transfer their ranges to top and right axes:
   connect(ui->widget->xAxis, SIGNAL(rangeChanged(QCPRange)), ui->widget->xAxis2, SLOT(setRange(QCPRange)));
   connect(ui->widget->yAxis, SIGNAL(rangeChanged(QCPRange)), ui->widget->yAxis2, SLOT(setRange(QCPRange)));

   // connect some interaction slots:
   connect(ui->widget, SIGNAL(titleDoubleClick(QMouseEvent*,QCPPlotTitle*)), this, SLOT(titleDoubleClick(QMouseEvent*,QCPPlotTitle*)));
   connect(ui->widget, SIGNAL(axisDoubleClick(QCPAxis*,QCPAxis::SelectablePart,QMouseEvent*)), this, SLOT(axisLabelDoubleClick(QCPAxis*,QCPAxis::SelectablePart)));
   connect(ui->widget, SIGNAL(legendDoubleClick(QCPLegend*,QCPAbstractLegendItem*,QMouseEvent*)), this, SLOT(legendDoubleClick(QCPLegend*,QCPAbstractLegendItem*)));

   // connect slot that shows a message in the status bar when a graph is clicked:
   connect(ui->widget, SIGNAL(plottableClick(QCPAbstractPlottable*,QMouseEvent*)), this, SLOT(graphClicked(QCPAbstractPlottable*)));

   // setup policy and connect slot for context menu popup:
   ui->widget->setContextMenuPolicy(Qt::CustomContextMenu);
   connect(ui->widget, SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(contextMenuRequest(QPoint)));

   ui->widget->setNoAntialiasingOnDrag(true);

   ui->widget->yAxis->setRange(0, (qreal)(1.0));
   QStringList strlist;
   strlist << "Red Data" << "Red Ambient Data" << "IR Data" << "IR Ambient"  << "Red Different" << "IR Different";
   for(int i = 0 ; i < graphNum ; i++)
      {
      ui->widget->addGraph();
      ui->widget->graph()->setName(strlist[i]);
      ui->widget->graph()->setPen(randomPen());
      ui->widget->graph()->setAdaptiveSampling(true);
      }
   ui->widget->axisRect()->setupFullAxesBox();
   pTimer = new QTimer;
   connect(pTimer, SIGNAL(timeout()), this, SLOT(onTimer()));
   connect(this, SLOT(close()), pTimer, SLOT(deleteLater()));
   pTimer->start(replotInterval);

   ///Test part
   ///
   freqTimer = new QTimer;
   connect(freqTimer, SIGNAL(timeout()), this, SLOT(onFreqTimer()));
   freqTimer->start(12000);

   prevVal=0;
   debugTimer.start();
   freqTime.start();
   pulseTick=0;
   peak=0;
   peak2=0;
   alreadyUsed = true;


   }

cplotter::~cplotter()
   {
   pTimer->stop();    delete ui;

   }


/** --------------------------------------------------------------------------------------
 * Класс сбора статистики
 */
class CStatistic
   {
public:
   /// КОНСТРУКТОР
   CStatistic(){ reset(); }
   /// Чтение статистики
   int getStat(qreal& mean, qreal& sigma)
      {
      mean  = Xm  / num;

      sigma = Xm * Xm;
      sigma = sigma / num;
      sigma = Xm2 - sigma;
      sigma = sigma / (num - 1);
      sigma = sqrt(sigma);
      return num;
      }
   /// Добавление новой точки
   int addPoint(qreal& X)
      {
      Xm  = Xm  + X;
      qreal X2 = X;
      Xm2 = Xm2 + X2 * X;
      ++num;
      return num;
      }

   /// Сброс счетчиков
   void reset()
      {
      Xm = 0; Xm2 = 0;
      num = 0;
      }

private:
   qreal Xm;
   qreal Xm2;
   qreal num;

   };

CStatistic  red, ir;

/**
 * @brief cplotter::addPoint - слот приемки точек
 * @param arr - массив с точками
 */
void cplotter::addPoint(const QByteArray &arr)
   {
   static uint points = 0;

   ++tick;
   ++points;

   uint24_t* pData = ( uint24_t* )( arr.data( ) + 4 );
   for(int i = 0 ; i < graphNum ; i++)
      {
      qreal val = (qreal)(pData[i]) / (qreal) 16777216.0;
      if (i==5)
         ui->widget->graph(i)->addData((qreal)tick, ((qreal)(pData[4]) / (qreal) 16777216.0)-val);
      }

   }

void cplotter::closeEvent(QCloseEvent)
   {
   deleteLater();
   }

void cplotter :: titleDoubleClick(QMouseEvent* event, QCPPlotTitle* title)
   {
   Q_UNUSED(event)
   // Set the plot title by double clicking on it
   bool ok;
   QString newTitle = QInputDialog::getText(this, "QCustomPlot example", "New plot title:", QLineEdit::Normal, title->text(), &ok);
   if (ok)
      {
      title->setText(newTitle);
      ui->widget->replot();
      }
   }

void cplotter :: axisLabelDoubleClick(QCPAxis *axis, QCPAxis::SelectablePart part)
   {
   // Set an axis label by double clicking on it
   if (part == QCPAxis::spAxisLabel) // only react when the actual axis label is clicked, not tick label or axis backbone
      {
      bool ok;
      QString newLabel = QInputDialog::getText(this, "QCustomPlot example", "New axis label:", QLineEdit::Normal, axis->label(), &ok);
      if (ok)
         {
         axis->setLabel(newLabel);
         ui->widget->replot();
         }
      }
   }

void cplotter :: legendDoubleClick(QCPLegend *legend, QCPAbstractLegendItem *item)
   {
   // Rename a graph by double clicking on its legend item
   Q_UNUSED(legend)
   if (item) // only react if item was clicked (user could have clicked on border padding of legend where there is no item, then item is 0)
      {
      QCPPlottableLegendItem *plItem = qobject_cast<QCPPlottableLegendItem*>(item);
      bool ok;
      QString newName = QInputDialog::getText(this, "QCustomPlot example", "New graph name:", QLineEdit::Normal, plItem->plottable()->name(), &ok);
      if (ok)
         {
         plItem->plottable()->setName(newName);
         ui->widget->replot();
         }
      }
   }

void cplotter :: selectionChanged()
   {
   /*
   normally, axis base line, axis tick labels and axis labels are selectable separately, but we want
   the user only to be able to select the axis as a whole, so we tie the selected states of the tick labels
   and the axis base line together. However, the axis label shall be selectable individually.

   The selection state of the left and right axes shall be synchronized as well as the state of the
   bottom and top axes.

   Further, we want to synchronize the selection of the graphs with the selection state of the respective
   legend item belonging to that graph. So the user can select a graph by either clicking on the graph itself
   or on its legend item.
  */

   // make top and bottom axes be selected synchronously, and handle axis and tick labels as one selectable object:
   if (ui->widget->xAxis->selectedParts().testFlag(QCPAxis::spAxis) || ui->widget->xAxis->selectedParts().testFlag(QCPAxis::spTickLabels) ||
       ui->widget->xAxis2->selectedParts().testFlag(QCPAxis::spAxis) || ui->widget->xAxis2->selectedParts().testFlag(QCPAxis::spTickLabels))
      {
      ui->widget->xAxis2->setSelectedParts(QCPAxis::spAxis|QCPAxis::spTickLabels);
      ui->widget->xAxis->setSelectedParts(QCPAxis::spAxis|QCPAxis::spTickLabels);
      }
   // make left and right axes be selected synchronously, and handle axis and tick labels as one selectable object:
   if (ui->widget->yAxis->selectedParts().testFlag(QCPAxis::spAxis) || ui->widget->yAxis->selectedParts().testFlag(QCPAxis::spTickLabels) ||
       ui->widget->yAxis2->selectedParts().testFlag(QCPAxis::spAxis) || ui->widget->yAxis2->selectedParts().testFlag(QCPAxis::spTickLabels))
      {
      ui->widget->yAxis2->setSelectedParts(QCPAxis::spAxis|QCPAxis::spTickLabels);
      ui->widget->yAxis->setSelectedParts(QCPAxis::spAxis|QCPAxis::spTickLabels);
      }

   // synchronize selection of graphs with selection of corresponding legend items:
   for(int i=0; i<ui->widget->graphCount(); ++i)
      {
      QCPGraph *graph = ui->widget->graph(i);
      QCPPlottableLegendItem *item = ui->widget->legend->itemWithPlottable(graph);
      if (item->selected() || graph->selected())
         {
         item->setSelected(true);
         graph->setSelected(true);
         }
      }
   }

void cplotter :: mousePress()
   {
   // if an axis is selected, only allow the direction of that axis to be dragged
   // if no axis is selected, both directions may be dragged

   if (ui->widget->xAxis->selectedParts().testFlag(QCPAxis::spAxis))
      ui->widget->axisRect()->setRangeDrag(ui->widget->xAxis->orientation());
   else if (ui->widget->yAxis->selectedParts().testFlag(QCPAxis::spAxis))
      ui->widget->axisRect()->setRangeDrag(ui->widget->yAxis->orientation());
   else
      ui->widget->axisRect()->setRangeDrag(Qt::Horizontal|Qt::Vertical);
   }

void cplotter :: mouseWheel()
   {
   // if an axis is selected, only allow the direction of that axis to be zoomed
   // if no axis is selected, both directions may be zoomed

   if (ui->widget->xAxis->selectedParts().testFlag(QCPAxis::spAxis))
      ui->widget->axisRect()->setRangeZoom(ui->widget->xAxis->orientation());
   else if (ui->widget->yAxis->selectedParts().testFlag(QCPAxis::spAxis))
      ui->widget->axisRect()->setRangeZoom(ui->widget->yAxis->orientation());
   else
      ui->widget->axisRect()->setRangeZoom(Qt::Horizontal|Qt::Vertical);
   }

///** ----------------------------------------------------------------


/** ----------------------------------------------------------------
 * @brief Слот установки интервала
 */
void cplotter :: setInterval()
   {
   QInputDialog* pqid = new QInputDialog(this);
   replotInterval = pqid->getInt(this, "Replot interval", "ms", replotInterval, 10, 2000);
   pTimer->setInterval(replotInterval);
   //    ui->widget->yAxis->setRange(0, (qreal)(1.0));
   }
/** ----------------------------------------------------------------
 * @brief Слот обработки установки положения легенды
 */
void cplotter :: removeSelectedGraph()
   {
   if (ui->widget->selectedGraphs().size() > 0)
      {
      //ui->widget->removeGraph(ui->widget->selectedGraphs().first());
      //ui->widget->replot();
      }
   }
/** ----------------------------------------------------------------
 * @brief Слот удаления всех графиков
 */
void cplotter :: removeAllGraphs()
   {
   //    for(int i = 0 ; i < ds_list.length() ; i++)
   //        delete (ds_list[i]);
   ui->widget->clearGraphs();
   ui->widget->replot();
   }
/** ----------------------------------------------------------------
 * @brief Слот обработки установки положения легенды
 */
void cplotter :: contextMenuRequest(QPoint pos)
   {
   QMenu *menu = new QMenu(this);
   menu->setAttribute(Qt::WA_DeleteOnClose);

   if (ui->widget->legend->selectTest(pos, false) >= 0) // context menu on legend requested
      {
      menu->addAction("Move to top left", this, SLOT(moveLegend()))->setData((int)(Qt::AlignTop|Qt::AlignLeft));
      menu->addAction("Move to top center", this, SLOT(moveLegend()))->setData((int)(Qt::AlignTop|Qt::AlignHCenter));
      menu->addAction("Move to top right", this, SLOT(moveLegend()))->setData((int)(Qt::AlignTop|Qt::AlignRight));
      menu->addAction("Move to bottom right", this, SLOT(moveLegend()))->setData((int)(Qt::AlignBottom|Qt::AlignRight));
      menu->addAction("Move to bottom left", this, SLOT(moveLegend()))->setData((int)(Qt::AlignBottom|Qt::AlignLeft));
      } else  // general context menu on graphs requested
      {
      //menu->addAction("Add graph", this, SLOT(addGraph()));
      if (ui->widget->selectedGraphs().size() > 0)
         menu->addAction("Remove selected graph", this, SLOT(removeSelectedGraph()));
      if (ui->widget->graphCount() > 0)
         menu->addAction("Remove all graphs", this, SLOT(removeAllGraphs()));
      menu->addAction("Set replot interval", this, SLOT(setInterval()));
      }

   menu->popup(ui->widget->mapToGlobal(pos));
   }
/** ----------------------------------------------------------------
 * @brief Слот обработки установки положения легенды
 */
void cplotter :: moveLegend()
   {
   if (QAction* contextAction = qobject_cast<QAction*>(sender())) // make sure this slot is really called by a context menu action, so it carries the data we need
      {
      bool ok;
      int dataInt = contextAction->data().toInt(&ok);
      if (ok)
         {
         ui->widget->axisRect()->insetLayout()->setInsetAlignment(0, (Qt::Alignment)dataInt);
         ui->widget->replot();
         }
      }
   }

/** ----------------------------------------------------------------
 * @brief Слот обработки клика мышки
 */
void cplotter :: graphClicked(QCPAbstractPlottable */*plottable*/)
   {
   //ui->statusBar->showMessage(QString("Clicked on graph '%1'.").arg(plottable->name()), 1000);
   }

/** ----------------------------------------------------------------
 * @brief Слот перерисовки графика
 */
void cplotter :: onTimer()
   {   //qDebug() << count; count =0;
   for(int i = 0 ; i < graphNum ; i++)
      ui->widget->graph(i)->removeDataBefore(tick - plotLen);

   ui->widget->xAxis->setRange(tick+10, plotLen, Qt::AlignRight);
   ui->widget->replot();

   }

void cplotter :: onFreqTimer()
   {
   qreal a;
   a=0.3*peak2+0.7*peak;
   //qDebug() << 5.*a;
   peak2=a;
   peak=0;
   }
