HEADERS += \
    $$PWD/qcustomplot.h \
    $$PWD/cplotter.h

SOURCES += \
    $$PWD/qcustomplot.cpp \
    $$PWD/cplotter.cpp

FORMS += \
    $$PWD/cplotter.ui
