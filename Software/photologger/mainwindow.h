#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtCore>
#include <QtGui>
#include <QtSerialPort>
#include <crc16.h>
#include <time.h>
#include <QLabel>

#include "cplotter.h"
#include "cppgcontrol.h"
#include "cspo2.h"
#include "configurator/linkergui.h"
#include "qblite-proto.h"
#include "Processing/processing.h"

#define CONFIG_MAX_COL 3

namespace Ui {
   class MainWindow;
   }

class CBLiteDecode;
class LinkerGUI;

enum sendFlags{
   SF_hasUpdatedRegs = 0,
   SF_hasResponseTimeOut,
   SF_hasResponse,
   SF_hasExpiredSendingDelay
   };

class MainWindow : public QMainWindow
   {
   Q_OBJECT
private:
   Ui::MainWindow *ui;
public:
   explicit MainWindow(QWidget *parent = 0);
   ~MainWindow();
   /// Настройки приложения

   friend LinkerGUI;
   LinkerGUI *linker;
   QSettings appSettings;
   QTimer *sendingDelayTimer, *responseOutTimer;
   bool flags[4]={false, false, false, false};
   void getWidgets(QList<QWidget *> &widgets);
   void loadSettings(void);
   void saveSettings(void);
   void writeCfgRegs(uint24_t *regs , uint8_t start = 1, uint8_t count = 29  ); // TODO уменьшить количество regs
   void sendShortCommand( char comm , uint8_t start , uint8_t stop );
   QList<QPair<BitAdress,int> > setTiming(int a);

signals:
   void putData(const QByteArray &arr);
   void sendSerial(QByteArray arr);
   void MainClose();
   void responseRecieved();
   void responseTimerOut();
   void requestDelayOut();
protected:
   void closeEvent(QCloseEvent *) { emit MainClose(); }

public slots:
   void sendAllRegistersResponse( ){ sendShortCommand( 'R' , 1 , AFE_REGS_COUNT - 1 ); }
   void sendStreamAfeResponse( bool setStartStream ) { sendShortCommand( 'S' , setStartStream ? 1 : 0 , 0 ); }
   void sendInitAfeResponse( ) { sendShortCommand( 'I' , 0 , 0 ); }

   void error_print(QString serr);
   void Print(QString str);

private slots:
   void on_actionAboute_triggered(){}
   void prepareSending(sendFlags e);
   void sendSettings();

   void slotPktErr(QByteArray & arr){arr=QByteArray();}
   void on_cbRF_LED1_currentIndexChanged(int){ printFc(); }
   void on_sbCF_LED1_valueChanged(int){ printFc(); }
   void on_cbRF_LED2_currentIndexChanged(int){ printFc(); }
   void on_sbCF_LED2_valueChanged(int){ printFc(); }

   void slotPkt(QByteArray arr);
   void on_actionRead_triggered();
   void on_actionWrite_triggered();
   void on_actionStream_triggered(bool checked);
   void on_actionSetFileName_triggered();
   void on_actionCapture_triggered(bool checked);
   void on_actionSaveToFile_triggered();
   void on_actionSpO2_triggered(bool checked);
   void on_actionRead_Conf_triggered();
   void on_cbLEDRANGEChanged(int);
   void printTableCell(BitAdress adr);


   void on_tableWidget_cellChanged(int row, int column);
   void on_actionStartProcessing_triggered(bool checked);

private:
   void appendSpecialFunctions();
   void printFc();
   void printTable(QStringList strList);


   QString generateName(QString str)
      {
      str = folderName + "/" + str + "_" +
            QDateTime :: currentDateTime().toString("yyMMddhhmmss") + ".csv";
      qDebug() << str;
      return str;
      }

   void fileWriteCSV(QByteArray arr);

   QSerialPort* pSerial;
   QByteArray array;
   QPointer<Processing> proc;
   QPointer<CBLiteDecode>  bl_in;
   QPointer<QLabel>        pInfoLb;
   QPointer<QLabel>        pFolderInfoLb;
   QPointer<QLabel>        pFileSizeLb;
   QPointer<cplotter>      wdtPlotter;
   QPointer<cSpO2>         wdtCSpO2;
   quint32                 pktcnt, errcnt;
   uint32_t                fileSize;
   ulong buf[7];


   CPPGControl ppg_control;

   qreal       currLED[2];
   QString     folderName;
   QString     folderNameCfg;
   QFile       file;
   };


#endif // MAINWINDOW_H
