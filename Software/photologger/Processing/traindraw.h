#ifndef TRAINDRAW_H
#define TRAINDRAW_H

#include <QColor>
#include <QPair>
#include <QList>
#include <QDebug>


enum ValueColorType
   {
   VCT_BlackAndWhite,
   VCT_Colored
   };

class TrainDraw
   {
   bool isBoundsLocked;
   int fColor[3][8]={{-1,-1,1,1,1,-1,-1,1},
                     {-1,-1,-1,-1,1,1,1,1},
                     {-1,1,1,-1,-1,-1,1,1}};


public:
   QList<QPair<qreal,qreal> > bounds;
//   qreal secBounds[3];

   TrainDraw();

   void init();
   void findBorders(QList<qreal> *li);
   bool canImproveBounds();
   void lockBounds(bool _shouldLock);
   QColor valueColor(qreal value, int row, ValueColorType VCT = VCT_Colored);
   };

#endif // TRAINDRAW_H
