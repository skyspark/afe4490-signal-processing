#ifndef PROCESSING_H
#define PROCESSING_H

#include <QByteArray>
#include <QList>

#include "processingtool.h"
#include "graphicwindow.h"

class Processing : public QObject
   {
   Q_OBJECT

   int counter;
   int maxSeqLen;
   int lastIndex;
   bool readyToRecieve;
   QTimer* timer;
   GraphicWindow *GW;
   QList<qreal> *storedValues;
   QList<qreal> *outputValues;
   QList<ProcessingTool> tools;
public:
   explicit Processing(QObject *parent = 0);

   void pushAndTrim(qreal _val);
   void addTool(int sequenceLength, PT::MethodType mt = PT::MT_Wavelet);
   void processingNext();
   void show(){GW->show();}

signals:
   void valuesReady();

public slots:
   void timerReady();
   void deleteLater();
   void addPoint(QByteArray arr);

   };

#endif // PROCESSING_H
