#ifndef CONVOLUTIONTOOL_H
#define CONVOLUTIONTOOL_H

#include <QList>
#include <QPixmap>
#include <QPainter>

namespace PT {
   enum MethodType
      {
      MT_Log,
      MT_FIRMatrix,
      MT_Average,
      MT_Wavelet
      };

   enum WaveLet
      {
      WL_MexicanHat
      };
   }

class ProcessingTool
   {
   int sequenceLength;
   int step, lastIndex;
   PT::MethodType method;
   qreal alpha[5];
   QList<qreal> *sequence;
   QList<qreal> *convoluted;
   qreal lastValue1, lastValue2;

   void pushAndTrim(qreal _val);
   qreal smoothMetLog(qreal _val);
   qreal smoothMetFIR(qreal _val);
   qreal smoothMetDiff(qreal _val);
   qreal smoothMetFlat(qreal _val);
   qreal waveLet(qreal x);
   qreal atR(qreal index);
   qreal findMax();
   qreal summOf(int i,int count, QList<qreal> &a);

public:
   ProcessingTool();
   ProcessingTool(int _sequenceLength, PT::MethodType _method);
   void init();

   void setAlpha();
   void setMParamethers(int iterations, int step);
   bool convolution();
   qreal smooth(qreal _val);
   int findInertialMax(qreal &primary, qreal carrier);
   qreal getDiff(qreal _val);
   void clearSequence();
   qreal calcPulse(int milliseconds);

   void setSequence(QList<qreal> *value);
   void setConvoluted(QList<qreal> *value);
   };

#endif // CONVOLUTIONTOOL_H
