#ifndef GRAPHICWINDOW_H
#define GRAPHICWINDOW_H

#include <QWidget>
#include <QGraphicsView>
#include <QGraphicsScene>
#include <QGraphicsItemGroup>
#include <QRectF>
#include <QTimer>
#include <random>
#include <QDebug>
#include <QWheelEvent>

#include "traindraw.h"

class GraphicWindow : public QGraphicsView
   {
   Q_OBJECT

   qreal scale;
   int currX;
   int maxColumns,maxRows;
   TrainDraw td;
   QList<qreal> *list;
   QGraphicsScene *gs;
   QPainter *p;

   void chop();

public:
   explicit GraphicWindow(QWidget *parent = 0);
   ~GraphicWindow();

   void setHeight(int i);
   void setList(QList<qreal> *value){ list = value;}
   void setMaxColumns(int value){ maxColumns = value; }
   void setMaxRows(int value) { maxRows = value*scale;}

public slots:
   void generate();
   void updateGraph();

signals:
   void generated();
   void updated();
   void scroll(int);

private:
   void wheelEvent(QWheelEvent *event);
   void resizeEvent(QResizeEvent *event);
   void deleteItemsFromGroup(QGraphicsItemGroup *group);
   };

#endif // GRAPHICWINDOW_H
