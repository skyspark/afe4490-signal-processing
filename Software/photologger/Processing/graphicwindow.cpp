#include "graphicwindow.h"

GraphicWindow::GraphicWindow(QWidget *parent)
   : QGraphicsView(parent)
   {
   scale=3;
   currX=0;

   this->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
   this->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
   this->setAlignment(Qt::AlignRight);
   this->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
   this->setMinimumWidth(200);

   gs=new QGraphicsScene();
   this->setScene(gs);
   this->maxColumns = this->gs->sceneRect().size().width();
   }

GraphicWindow::~GraphicWindow() {
   delete gs;
   this->close();
   }

void GraphicWindow::setHeight(int i)
   {
   this->setMaximumHeight(i*scale);
   }

void GraphicWindow::updateGraph()
   {
   if(height()>list->count()*scale) setMaximumHeight(list->count()*scale);
   QPixmap *pix = new QPixmap(1,list->count()*scale);
   p = new QPainter(pix);
   QPen pen;
   QBrush brush(Qt::SolidPattern);
   QColor col;
   td.findBorders(list);
   for(int i =0; i<(list->count()); ++i){
      col = td.valueColor(list->at(i),i);
      pen.setColor(col);
      brush.setColor(col);
      p->setPen(pen);
      p->setBrush(brush);
      p->drawRect(0,i*scale,1,scale);
      }
   gs->addPixmap(*pix);
   for(auto a: gs->items()){
      a->moveBy(-1,0);
      }
   if(++currX>100) chop();
   delete p;
   delete pix;
   }


void GraphicWindow::wheelEvent(QWheelEvent *event)
   {
//   td.lockBounds(event->delta() > 0);
   emit scroll(event->delta()*2);

   QGraphicsView::wheelEvent(event);
   }

void GraphicWindow::chop()
   {
   for( QGraphicsItem *item : gs->items()) {
      if (item->pos().x() < this->frameRect().width()*(-1)){
         delete item;
         gs->invalidate();
         }
      }
   currX=0;
   }

void GraphicWindow::resizeEvent(QResizeEvent *event)
   {
   this->maxColumns = this->size().width();
   chop();
   gs->setSceneRect(gs->itemsBoundingRect());
   this->window()->setMinimumHeight(list->count()*scale);
   QGraphicsView::resizeEvent(event);
   }

void GraphicWindow::generate()
   {
   for(int i=0; i<maxRows/scale; ++i){
      qreal val = 0.8*list->at(i) + 0.2*(rand()%2000);
      list->replace(i,val);
      }
   }
