#include <QDebug>
#include "processing.h"
#include "ppgproperties.h"

Processing::Processing(QObject *parent) : QObject(parent)
   {
   int lvls=301;
   counter=0;
   storedValues = new QList<qreal>;
   outputValues = new QList<qreal>;
   GW = new GraphicWindow();
   GW->setList(outputValues);
   GW->setHeight(lvls);
   timer = new QTimer;
   lastIndex =0;
   addTool(lvls);
   readyToRecieve=true;
//   connect(GW, &GraphicWindow::scroll, this, [=](int i){tools[0].changeSeqLength(i);});
//   connect(timer,&QTimer::timeout, this, &Processing::timerReady);
//   timer->start(1);
   }

void Processing::addTool(int sequenceLength, PT::MethodType mt)
   {
   if(sequenceLength>maxSeqLen) {
      maxSeqLen = sequenceLength;
      }
   tools.append(ProcessingTool(sequenceLength,mt));
   tools.last().setSequence(storedValues);
   tools.last().setConvoluted(outputValues);
   }

void Processing::addPoint(QByteArray arr)
   {
   if(!readyToRecieve) return;
   readyToRecieve = false;
   uint24_t* pData = ( uint24_t* )( arr.data()+ 1 );
   qreal val[6];
   for(int i = 0 ; i < 6 ; i++)
      {
      val[i] = (qreal)(pData[i]) / (qreal)16777216.0;
      }
   qreal outVal = val[4]-val[5];

   pushAndTrim(outVal);
//   ++counter;
//   if(counter==20) {qDebug() << outVal; counter=0;}
   processingNext();
   }

void Processing::pushAndTrim(qreal _val)
   {
   lastIndex=(lastIndex<1000000)?lastIndex+1:0;
   if (storedValues->length() >= maxSeqLen) storedValues->removeFirst();
   storedValues->append(_val);
   }

void Processing::processingNext()
   {
   bool ready;
   for(int i =0; i<tools.length(); ++i){
      ready = tools[i].convolution();
      if(ready) GW->updateGraph();
      }
   readyToRecieve = true;
   }

void Processing::timerReady()
   {
   readyToRecieve=true;
   }

void Processing::deleteLater()
   {
   delete timer;
   delete GW;
   storedValues->clear();
   delete storedValues;
   outputValues->clear();
   delete outputValues;
   }
