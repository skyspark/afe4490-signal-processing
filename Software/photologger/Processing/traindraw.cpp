#include "traindraw.h"

TrainDraw::TrainDraw()
   {
   init();
   }

void TrainDraw::init()
   {
   }

void TrainDraw::findBorders(QList<qreal> *li)
   {
   if(!isBoundsLocked)
      for(int i=0; i<li->count(); ++i){
         if(bounds.count() == i){
            QPair<qreal,qreal> a(-0.00001,0.00001);
            bounds.append(a);
            }
         else
            if (li->at(i)< INFINITY && li->at(i) >-INFINITY){
               if(bounds.at(i).first > li->at(i) ) bounds[i].first = bounds[i].first*0.95 + li->at(i)*0.15;
               else if(i>0 && i<li->count()-1) bounds[i].first=bounds[i].first*0.979 + bounds[i-1].first*0.01 + bounds[i+1].first*0.01;
               if(bounds.at(i).second < li->at(i) ) bounds[i].second = bounds[i].second*0.95 + li->at(i)*0.15;
               else if(i>0 && i<li->count()-1) bounds[i].second=bounds[i].second*0.979 + bounds[i-1].second*0.01 + bounds[i+1].second*0.01;
               }
         }
   }

bool TrainDraw::canImproveBounds()
   {
   //   return bounds[2]>0.00001;
   }

void TrainDraw::lockBounds(bool _shouldLock)
   {
   isBoundsLocked = _shouldLock;
   }


QColor TrainDraw::valueColor(qreal value, int row, ValueColorType VCT)
   {
   qreal range = bounds.at(row).second - bounds.at(row).first;
   qreal rgb[3]={0,0,0};
   int colorVal=0;
   switch(VCT){
      case VCT_Colored:
         {
         colorVal=((256.*7.)/range)*(value - bounds.at(row).first);
         int j = colorVal/256. + 1;
         for(int i=0; i<3; i++){
            rgb[i]=127.5*(fColor[i][j-1]+1);
            if (fColor[i][j-1]!=fColor[i][j])
               rgb[i]+=fColor[i][j]*(colorVal%256);
            if(rgb[i]>255) rgb[i]=255;
            if(rgb[i]<0) rgb[i]=0;
            }
         } break;
      case VCT_BlackAndWhite:
         {
         colorVal = (256./range)*(value - bounds.at(row).first);
         if(colorVal>255) colorVal=255;
         if(colorVal<0) colorVal=0;
         for(int i=0;i<3;++i) rgb[i] = colorVal;
         } break;
      default: break;
      }
   return QColor(rgb[0],rgb[1],rgb[2]);
   }


