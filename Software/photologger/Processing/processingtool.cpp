#include <qdebug.h>
#include <cassert>
#include "processingtool.h"


void ProcessingTool::init()
   {
   setAlpha();
   step = 3;
   lastIndex =0;
   sequenceLength=10;
   lastValue1=0;
   lastValue2=0;
   }

ProcessingTool::ProcessingTool()
   {
   init();
   method=PT::MT_Log;
   }

ProcessingTool::ProcessingTool(int _sequenceLength, PT::MethodType _method)
   {
   //   bool MethodRequiresConvolutionBase = (_method == PT::MT_FIRMatrix);
   //   if (MethodRequiresConvolutionBase) assert(MethodRequiresConvolutionBase);
   init();
   sequenceLength=_sequenceLength;
   method=_method;
   }


qreal ProcessingTool::smooth(qreal _val)
   {
   qreal out;
   switch(method) {
      case PT::MT_Log: {
         out = smoothMetLog(_val);
         } break;
      case PT::MT_Wavelet: {
         ///INFO: function is not consistent
         ///out = smoothMetFIR(_val);
         } break;
      case PT::MT_Average:{
         out = smoothMetFlat(_val);
         } break;
      default: qDebug() << "ProcessingTool->Smooth() case:default" << endl; break;
      }
   return out;
   }

int ProcessingTool::findInertialMax(qreal &primary, qreal carrier)
   {
   lastValue2=lastValue1;
   findMax();
   if (lastValue1 > lastValue2)
      return 1;
   else
      {
      qreal alp = alpha[1];

      if ((lastValue1 - lastValue2)<1e-07 || (lastValue1 - lastValue2)>-1e-07)
         carrier=1.2*primary;

      lastValue1=carrier * alp + lastValue1 * (1.-alp);
      primary = lastValue1;
      return 0;
      }
   }

qreal ProcessingTool::findMax()
   {
   foreach (auto a, *sequence) {
      if (a > lastValue1) lastValue1=a;
      }
   return lastValue1;
   }

qreal ProcessingTool::getDiff(qreal _val)
   {
   qreal ret;
   ret = _val - lastValue1;
   lastValue1 = _val;
   return ret;
   }

qreal ProcessingTool::smoothMetLog(qreal _val)
   {
   lastValue2=_val*alpha[0]+lastValue2*(1.-alpha[0]);
   return lastValue2;
   }

//qreal ProcessingTool::smoothMetFIR(qreal _val)
//   {
////   pushAndTrim(_val);
////   if (sequence->length()<sequenceLength) return 0;
////   return convolution(indexConvolutionVector);
//   }

qreal ProcessingTool::smoothMetFlat(qreal _val)
   {
   if (sequence->length() == sequenceLength)
      return summOf(0, sequenceLength, *sequence)/qreal(sequenceLength);
   else return 0;
   }

qreal ProcessingTool::waveLet(qreal x){
   return 2.0/pow(3.0,0.5)*pow(M_PI,-0.25)*(1.0-x*x)*exp(-(x*x)/2.0);
   }

qreal ProcessingTool::atR(qreal index){
   return (sequence->at((int)index) + sequence->at((int)index+1))/(index-(int)index);
   }

bool ProcessingTool::convolution()
   {
   if (sequence->length()<sequenceLength) return false;
   int lvl=0;
   int stepLvl = step*lvl;
   while (stepLvl*2 + step*15  < sequenceLength)
      {
      qreal value=0;
      int newRange = sequenceLength-stepLvl*2.0;
      for(int i=0; i<(newRange); i++)
         value += sequence->at(i+stepLvl) * waveLet((i-newRange/2)*(10.0/(newRange)));
      if (convoluted->length()<lvl+1){
         convoluted->append(1);
         }
      convoluted->replace(lvl,value);
      lvl++;
      stepLvl+=step;
      }
   return true;
   }

qreal ProcessingTool::calcPulse(int milliseconds)
   {
   return qreal(60.*1000.)/qreal(milliseconds);
   }

void ProcessingTool::setSequence(QList<qreal> *value)
   {
   sequence = value;
   }

void ProcessingTool::setConvoluted(QList<qreal> *value)
   {
   convoluted = value;
   }

qreal ProcessingTool::summOf(int i, int count, QList<qreal> &a)
   {
   qreal summ=0;
   for(int j=0; j<count; j++) {
      summ+=a.at(j+i);
      }
   return summ;
   }

void ProcessingTool::clearSequence()
   {
   sequence->clear();
   }

void ProcessingTool::setAlpha()
   {
   alpha[0]=0.08;
   alpha[1]=0.001;
   alpha[2]=0.006;
   alpha[3]=1;
   alpha[4]=1;
   }
