INCLUDEPATH += $$PWD

HEADERS += \
    $$PWD/crc16.h \
    $$PWD/crc32.h

SOURCES += \
    $$PWD/crc16.c \
    $$PWD/crc32.c
