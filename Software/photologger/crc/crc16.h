/**
 * @file crc16.h
 *
 */

#ifndef CRC16_H_
#define CRC16_H_

/// ��������� ������� CRC
#define CRC16_INIT  0xFFFF

#ifdef __cplusplus
extern "C"
{
#endif
/**
 * ��������� CRC �� �������� 0xA001
 * @param crc - ���������� ������� CRC
 * @param a - ������� ����
 * @return ����������� �������� CRC
 */
uint_least16_t crc16_update_poly( uint_least16_t crc , uint8_t a );

/**
 * ��������� CRC �����
 * @param crc - ���������� ������� CRC
 * @param a - ������� ����
 * @param len ����� �����
 * @return ����������� �������� CRC
 */
uint_least16_t crc16_update_blk( uint_least16_t crc , uint8_t* a , uint32_t len );
#ifdef __cplusplus
}
#endif
#endif /* CRC16_H_ */
