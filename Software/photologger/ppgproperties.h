#ifndef PPGPROPERTIES_H
#define PPGPROPERTIES_H

#include <stdint.h>
#include <QDebug>

/** -------------------------------------------------------------------
 * Registers adresses @{
 */
// --------------------------------------------------------------------------------------
#define CONTROL0		0x00
// --------------------------------------------------------------------------------------
// LED2 SAMPLE
#define LED2STC			0x01  // START SAMPLE               = 3 *cell + delay
#define LED2ENDC        0x02  // STOP SAMPLE                = 4 *cell - 2
#define LED2LEDSTC		0x03  // ON                         = 3 *cell
#define LED2LEDENDC		0x04  // OFF                        = 4 *cell - 1
#define ALED2STC        0x05  // START AMBIENT SAMPLE       = delay (cirlce)
#define ALED2ENDC       0x06  // STOP  AMBIENT SAMPLE       = 1 *cell - 2     (cirlce)
// --------------------------------------------------------------------------------------
// LED1 SAMPLE
#define LED1STC			0x07  // START SAMPLE               = 1 *cell + delay
#define LED1ENDC     	0x08  // STOP  SAMPLE               = 2 *cell - 2
#define LED1LEDSTC		0x09  // ON                         = 1 *cell
#define LED1LEDENDC		0x0a  // OFF                        = 2 *cell - 1
#define ALED1STC     	0x0b  // START AMBIENT SAMPLE       = 2 *cell + delay
#define ALED1ENDC    	0x0c  // STOP  AMBIENT SAMPLE       = 3 *cell - 2
// --------------------------------------------------------------------------------------
// LED2 CONV
#define LED2CONVST		0x0d  // START CONV                 = gap
#define LED2CONVEND		0x0e  // STOP  CONV                 = 1 *cell - 1
#define ALED2CONVST		0x0f  // START CONV                 = 2 *cell + gap
#define ALED2CONVEND    0x10  // STOP  CONV                 = 3 *cell - 1
// --------------------------------------------------------------------------------------
// LED1 CONV
#define LED1CONVST		0x11  // START CONV                 = 1 *cell + gap
#define LED1CONVEND		0x12  // STOP  CONV                 = 2 *cell - 1
#define ALED1CONVST		0x13  // START CONV                 = 3 *cell + gap
#define ALED1CONVEND    0x14  // STOP  CONV                 = 4 *cell - 1
// --------------------------------------------------------------------------------------
//  RESET 0
#define ADCRSTCNT0		0x15  // RESET START                = 0 *cell
#define ADCRSTENDCT0    0x16  // RESET STOP                 = 0 *cell + gap - 1
//  RESET 1
#define ADCRSTCNT1		0x17  // RESET START                = 1 *cell
#define ADCRSTENDCT1    0x18  // RESET STOP                 = 1 *cell + gap - 1
//  RESET 2
#define ADCRSTCNT2		0x19  // RESET START                = 2 *cell
#define ADCRSTENDCT2 	0x1a  // RESET STOP                 = 2 *cell + gap - 1
//  RESET 3
#define ADCRSTCNT3		0x1b  // RESET START                = 3 *cell
#define ADCRSTENDCT3 	0x1c  // RESET STOP                 = 3 *cell + gap - 1
// PRESCALER
#define PRPCOUNT        0x1d


/** @} */
/// @brief Maximum sample frequency
#define MAX_SAMPLE_FREQ_HZ      ((uint32_t) 500)
/// @brief Default sample frequency
#define DEF_SAMPLE_FREQ_HZ      ((uint32_t) 500)
/// @brief Minimum sample frequency
#define MIN_SAMPLE_FREQ_HZ      ((uint32_t) 50)
/// @brief Main clock frequency
#define CLK_FREQ_HZ             ((uint32_t) 4000000)
/// @brief
#define CONV_TIME_GAP           6
/// @brief Signal settling time
#define CONV_START_DELAY        80
/// @brief Count of timing config registers
#define TIMING_REG_COUNT        (PRPCOUNT - LED2STC)
/// @brief Maximum AFE registers index
#define MAX_AFE_REG_NUM         (48)
/// @brief Count of all AFE registers
#define COUNT_AFE_REG           (MAX_AFE_REG_NUM + 1)

#define COUNT_WRITEBLE_AFE_RES  ( ( offsetof( afe_regs , alarm ) / 3 ) - LED2STC )



#pragma pack(push, 1)
typedef union UINT24_T
   {
   uint8_t     raw[ 3 ];
   uint32_t    value : 24;

   UINT24_T operator =  ( uint32_t in ) { value = in;  return *this; }
   UINT24_T operator &= ( uint32_t in ) { value &= in; return *this; }
   UINT24_T operator |= ( uint32_t in ) { value |= in; return *this; }
   operator uint32_t( ) { return ( uint32_t )( ( raw[ 2 ] << 16 ) | ( raw[ 1 ] << 8 ) | raw[ 0 ] ); }
   operator int( )      { return ( int )( ( raw[ 2 ] << 16 ) | ( raw[ 1 ] << 8 ) | raw[ 0 ] ); }
   operator uint8_t*( ) { return raw; }
   operator QString()   { return QString("%1").arg(value,0,2); }
   operator qreal( )    { return ( qreal ) operator uint32_t( ); }

   } uint24_t;


#pragma pack(pop)

#pragma pack(push, 1)
struct control0_t
   {
   uint8_t                     :8;
   uint8_t                     :8;
   uint8_t                     :4;
   uint8_t SW_RST              :1;
   uint8_t DIAG_EN             :1;
   uint8_t TIM_COUNT_RST       :1;
   uint8_t SPI_READ            :1;
   };
#pragma pack(pop)

#pragma pack(push, 1)
struct control1_t
   {
   uint8_t                     :8;
   uint8_t                     :4;
   uint8_t CLKALMPIN           :3;
   uint8_t TIMEREN             :1;
   uint8_t NUMAV               :8;
   };
#pragma pack(pop)

#pragma pack(push, 1)
struct tiagain_t
   {
   uint8_t                     :8;
   uint8_t ENSEPGAN            :1;
   uint8_t STAGE2EN1           :1;
   uint8_t                     :3;
   uint8_t STG2GAIN            :3;
   uint8_t cf_led              :5;
   uint8_t rf_led              :3;
   };
#pragma pack(pop)

#pragma pack(push, 1)
struct tiaambgain_t
   {
   uint8_t                     :4;
   uint8_t AMBDAC              :4;
   uint8_t FLTRCNRSEL          :1;
   uint8_t STAGE2EN2           :1;
   uint8_t                     :3;
   uint8_t STG2GAIN            :3;
   uint8_t cf_led              :5;
   uint8_t rf_led              :3;
   };
#pragma pack(pop)

#pragma pack(push, 1)
struct ledcntrl_t
   {
   uint8_t                     :6;
   uint8_t LEDRANGE            :2;
   uint8_t LED1                :8;
   uint8_t LED2                :8;
   };
#pragma pack(pop)

#pragma pack(push, 1)
struct control2_t
   {
   uint8_t                     :4;
   uint8_t                     :1; //RESERVED
   uint8_t TX_REF              :2; //ref0 | ref1<<=1
   uint8_t RST_CLK_ON_PL_ALM   :1;
   uint8_t EN_ADC_BYP          :1;
   uint8_t                     :3;
   uint8_t TXBRGMOD            :1;
   uint8_t DIGOUT_TRISTATE     :1;
   uint8_t XTALDIS             :1;
   uint8_t EN_SLOW_DIAG        :1;
   uint8_t                     :5;
   uint8_t PDNTX               :1;
   uint8_t PDNRX               :1;
   uint8_t PDNAFE              :1;
   };
#pragma pack(pop)

#pragma pack(push, 1)
struct val_t
   {
   uint24_t VAL;
   };
#pragma pack(pop)

#pragma pack(push, 1)
struct diag_t
   {
   uint8_t                     :8;
   uint8_t                     :3;
   uint8_t PD_ALM              :1;
   uint8_t LED_ALM             :1;
   uint8_t LED1OPEN            :1;
   uint8_t LED2OPEN            :1;
   uint8_t LEDSC               :1;
   uint8_t OUTNSHGND           :1;
   uint8_t OUTPSHGND           :1;
   uint8_t PDOC                :1;
   uint8_t PDSC                :1;
   uint8_t INNSCGND            :1;
   uint8_t INPSCGND            :1;
   uint8_t INNSCLED            :1;
   uint8_t INPSCLED            :1;
   };
#pragma pack(pop)

#pragma pack(push, 1)
struct alarm_t
   {
   uint16_t                    :16;
   uint8_t ALMPINCLKEN         :1;
   };
#pragma pack(pop)

#pragma pack(push, 1)
struct led_time_t
   {
   uint8_t                     :8;
   uint16_t      time          :16;
   led_time_t operator =(uint16_t in){ time = in; return *this; }
   operator uint16_t(){ return time; }
   };
#pragma pack(pop)

///Значения предельных токов светодиодов
typedef enum CUR_RANGE_E
   {
   CUR_FULL    = 0,
   CUR_HALF    = 1,
   CUR_FULLALT = 2,
   CUF_OFF     = 3
   }cur_range_t;

///Значения опорного напряжения
typedef enum UREF_RANGE_E
   {
   UREF_075    = 0,
   UREF_05     = 1,
   UREF_10     = 2,
   UREF_OFF    = 3
   }uref_range_t;

///Возможные значения R обратной связи
typedef enum RF_VALUES_T
   {
   RF_500k  = 0,
   RF_250k  = 1,
   RF_100k  = 2,
   RF_50k   = 3,
   RF_25k   = 4,
   RF_10k   = 5,
   RF_1M    = 6,
   RF_NO    = 7
   }rf_value_t;

///Значения усилений второго каскада
typedef enum STG2GAINS_E
   {
   GAIN_0dB     = 0,
   GAIN_3dB     = 1,
   GAIN_6dB     = 2,
   GAIN_9dB     = 3,
   GAIN_12dB    = 4
   }stg2gains_t;


enum
{
    UINT24SIZE              = sizeof( uint24_t ),
    AFE_REGS_COUNT          = COUNT_AFE_REG,
    AFE_REGS_BYTE_SIZE      = UINT24SIZE * AFE_REGS_COUNT
};

#pragma pack(push, 1)
union afe_regs
   {
   uint24_t raw[AFE_REGS_COUNT];
   uint8_t  byteArray[ AFE_REGS_BYTE_SIZE ];
   struct
      {
      control0_t      control0;
      led_time_t      led2stc;
      led_time_t      led2endc;
      led_time_t      led2ledstc;
      led_time_t      led2ledendc;
      led_time_t      aled2stc;
      led_time_t      aled2endc;
      led_time_t      led1stc;
      led_time_t      led1endc;
      led_time_t      led1ledstc;
      led_time_t      led1ledendc;
      led_time_t      aled1stc;
      led_time_t      aled1endc;
      led_time_t      led2convst;
      led_time_t      led2convendc;
      led_time_t      aled2convst;
      led_time_t      aled2convendc;
      led_time_t      led1convst;
      led_time_t      led1convendc;
      led_time_t      aled1convst;
      led_time_t      aled1convendc;
      led_time_t      adcrstcnt0;
      led_time_t      adcendct0;
      led_time_t      adcrstct1;
      led_time_t      adcendct1;
      led_time_t      adcrstct2;
      led_time_t      adcendct2;
      led_time_t      adcrstct3;
      led_time_t      adcendct3;
      led_time_t      prpct;
      control1_t      control1;
      uint24_t        spare1;
      tiagain_t       tiagain;
      tiaambgain_t    tiaambgain;
      ledcntrl_t      ledcntrl;
      control2_t      control2;
      uint24_t        spare2;
      uint24_t        spare3;
      uint24_t        spare4;
      uint24_t        reserved1;
      uint24_t        reserved2;
      alarm_t         alarm;
      val_t           led2val;
      val_t           aled2val;
      val_t           led1val;
      val_t           aled1val;
      val_t           led2absval;
      val_t           led1absval;
      diag_t          diag;
      };
   };
#pragma pack(pop)

#endif // PPGPROPERTIES_H
