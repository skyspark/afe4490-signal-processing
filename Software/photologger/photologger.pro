#-------------------------------------------------
#
# Project created by QtCreator 2015-03-03T11:17:13
#
#-------------------------------------------------

QT       += core gui serialport opengl
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets printsupport


CONFIG += c++11
win:{
QMAKE_CXXFLAGS += -std=c++11 -opengl
QMAKE_LFLAGS   += -std=c++11 -opengl
}
unix: !macx {
QMAKE_CXXFLAGS += -std=c++11 -opengl -std=gnu++11
}


##Defines
#win:{
#   DEFINES += PORT_NAME=\"COM5\"
#}
#!win:{
#   DEFINES += PORT_NAME=\"ttyUSB0\"
#}

TARGET = photologger
TEMPLATE = app

include(crc/crc.pri)
include(qcustomplot/qcustomplot.pri)
include(logout/logout.pri)
include(cspo2/cspo2.pri)
include(configurator/configurator.pri)
include(Processing/Processing.pri)

INCLUDEPATH += crc qcustomplot logout cspo2


SOURCES +=  main.cpp\
            mainwindow.cpp \
            cserialport.cpp \
            cppgcontrol.cpp



HEADERS  += cserialport.h \
            cppgcontrol.h \
            mainwindow.h \
            ppgproperties.h \
    qblite-proto.h


FORMS    += mainwindow.ui

RESOURCES += icons.qrc

SUBDIRS += tests

DISTFILES += \
    ../../../doxygen/build/bin/html/bdwn.png
