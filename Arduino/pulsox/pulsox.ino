/** ---------------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------------
                                    AFE4490:
   1  - SCK; 2  - ENA; 3  - SOMI; 4  - SIMO; 5  - STE; 6  - RDY; 7  - RST.
    ---------------------------------------------------------------------------------------------
                                 ARDUINO NANO:
   13 - SCK; 5  - ENA; 12 - SOMI; 11 - SIMO; 9  - STE; 2  - RDY; 4  - RST.
   ---------------------------------------------------------------------------------------------
                                  LIFEPATH MIDDLE BOARD
   4  - SCK; 2  - ENA; 6  - SOMI; 5  - SIMO; 3  - STE; 7  - RDY; -  - ---; 1  - ADC-CS
   --------------------------------------------------------------------------------------------- */
#include <cblite.h>
#include <string.h>
#include <SPI.h>
#include "AFE4490.h"
#include <math.h>
#include "crc16.h"

//#define BLITE_DBG

//----------------------------------------------------------------------------------------------
AFE       afe; // Класс оксиметра
CBLite    bl;  // Класс обработчика протоколаSerial
//----------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------
#pragma pack(push, 1)
typedef struct WRITE_S
{
  unsigned char    start;
  unsigned char    count;
  uint24_t         buf[ DIAG + 1 ];
} write_t;
#pragma pack(pop)

//----------------------------------------------------------------------------------------------
#define RAW_MAX_LENGTH    ( sizeof( write_t ) )
//----------------------------------------------------------------------------------------------
#pragma pack(push, 1)
typedef union DATA_U
{
  write_t data;
  unsigned char raw[ RAW_MAX_LENGTH ];
} data_t;
#pragma pack(pop)
//----------------------------------------------------------------------------------------------
#pragma pack(push, 1)
typedef struct AFE_MEAS_S
{
    uint24_t  meas[ 6 ];
    uint32_t  ts;    
} afe_meas_pkt_t;
#pragma pack(pop)
//----------------------------------------------------------------------------------------------
/// Флаг реaкции на прерывание
static volatile int ISR_state = LOW;
static volatile int f_stream = LOW;
void afe_isr( void ) {
  ISR_state = HIGH;
};
static int isr_event( ) {
  noInterrupts();
  int res = ISR_state;
  ISR_state = 0;
  interrupts();
  return res;
}
//----------------------------------------------------------------------------------------------
void setup()
{
  afe.afe_power( true );
  afe.afe_init( );
  afe.afe_set_rdy_cb( afe_isr );
  bl.begin( );
}

//----------------------------------------------------------------------------------------------
/// Отправка измерений
void send_afe( )
{
  union
  {
    afe_meas_pkt_t pkt;
    uint8_t        raw[ sizeof( afe_meas_pkt_t ) ];
  } res;
  afe.afe_open( AFE::RDMODE );
  for ( int i = 0 ; i < 6 ; i++ ) res.pkt.meas[ i ] = afe.afe_read( LED2VAL + i );
  res.pkt.ts = millis();
  bl.writepkt( 'X' , res.raw , sizeof( afe_meas_pkt_t ) );
  Serial.flush();
}

//----------------------------------------------------------------------------------------------
/// Событие по приему симыволов
void serialEvent()
{
  int res;
  data_t DATA;
  unsigned char start, count;

  char CMD;
  /// Пробуем вычитать пакет из потокa
  res = bl.readpkt( &CMD , (char*)DATA.raw , RAW_MAX_LENGTH  );
  count = DATA.data.count;
  start = DATA.data.start;

  if ( !res ) return;

  int i;
  ///// ОБРАБОТЧИК КОМАНД
  switch ( CMD )
  {
    /// Записываем регистры:       [0]-offset [1]-records [2...] records byte
    case 'W':

      afe.afe_open( AFE::WRMODE );
      
      for ( i = 0 ; i < count ; i++ )  afe.afe_write( start + i , DATA.data.buf[ i ] );

      break;
    /// Читаем регистры:           [0]-offset [1]-records [2...] records byte
    case 'R':
      afe.afe_open( AFE::RDMODE );
      for ( i = 0 ; i < count ; i++ ) DATA.data.buf[ i ]  = afe.afe_read( start + i );
      DATA.raw[ 1 ] = count;
      DATA.raw[ 0 ] = start;
      bl.writepkt( 'R' , DATA.raw ,  2 + ( count * sizeof( uint24_t ) ) );
      Serial.flush();

      break;
    /// Запуск потоковой передачи
    case 'S':
      f_stream = start;
      break;
    /// Управление питанием
    case 'I':
      bl.writepkt( 'A' , DATA.raw , 2 );  Serial.flush();
      afe.afe_power( true );
      afe.afe_init( );
      break;
  };
  bl.writepkt( 'A' , DATA.raw , 2 );    Serial.flush();
}

//----------------------------------------------------------------------------------------------
void loop( )
{
  // Send measurements
  if ( isr_event() == HIGH && f_stream == HIGH ) send_afe( );

}





