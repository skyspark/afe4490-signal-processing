
#ifndef AFE4490_H_
#define AFE4490_H_


#include <stdint.h>
#include <Arduino.h>

#include <SPI.h>

/* Register definitions */

#define CONTROL0		0x00
#define LED2STC			0x01
#define LED2ENDC		0x02
#define LED2LEDSTC		0x03
#define LED2LEDENDC		0x04
#define ALED2STC		0x05
#define ALED2ENDC		0x06
#define LED1STC			0x07
#define LED1ENDC		0x08
#define LED1LEDSTC		0x09
#define LED1LEDENDC		0x0a
#define ALED1STC		0x0b
#define ALED1ENDC		0x0c
#define LED2CONVST		0x0d
#define LED2CONVEND		0x0e
#define ALED2CONVST		0x0f
#define ALED2CONVEND	0x10
#define LED1CONVST		0x11
#define LED1CONVEND		0x12
#define ALED1CONVST		0x13
#define ALED1CONVEND	0x14
#define ADCRSTCNT0		0x15
#define ADCRSTENDCT0	0x16
#define ADCRSTCNT1		0x17
#define ADCRSTENDCT1	0x18
#define ADCRSTCNT2		0x19
#define ADCRSTENDCT2	0x1a
#define ADCRSTCNT3		0x1b
#define ADCRSTENDCT3	0x1c
#define PRPCOUNT		0x1d
#define CONTROL1		0x1e
#define SPARE1			0x1f
#define TIAGAIN			0x20
#define TIA_AMB_GAIN	0x21
#define LEDCNTRL		0x22
#define CONTROL2		0x23
#define SPARE2			0x24
#define SPARE3			0x25
#define SPARE4			0x26
#define SPARE4			0x26
#define RESERVED1		0x27
#define RESERVED2		0x28
#define ALARM			0x29
#define LED2VAL			0x2a
#define ALED2VAL		0x2b
#define LED1VAL			0x2c
#define ALED1VAL		0x2d
#define LED2ABSVAL		0x2e
#define LED1ABSVAL		0x2f
#define DIAG			0x30


#pragma pack(push, 1)
typedef union UINT24_T
{
  uint8_t     raw[3];
  uint32_t    value   : 24;

  UINT24_T operator =(uint32_t in) {
    value = in;
    return *this;
  }
  UINT24_T operator &= (uint32_t in) {
    value &= in;
    return *this;
  }
  UINT24_T operator |= (uint32_t in) {
    value |= in;
    return *this;
  }
} uint24_t;

typedef union CONVU24
{
  uint32_t  u32;
  struct
  {
    uint24_t  u24;
    uint8_t   dummy;
  };
} convu24_t;


#pragma pack(pop)

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/** --------------------------------------------------------------------------------------------------
 */
class AFE
{
public:
	AFE( int somi = 12 , int simo = 11, int sclk = 13, int spiste = 9, int rdy = 2, int rst = 4, int ena = 5 ):
		SOMI( somi ) , SIMO( simo ) , SCLK( sclk ) , SPISTE( spiste ) , RDY( rdy ) , RST( rst ) , ENA( ena ){ } 
 
	 int SOMI; 
	 int SIMO; 
	 int SCLK;
	 int SPISTE; 

	 int RDY; 
	 int RST; 
	 int ENA;
	             
  /**-------------------------------------------------------------------------------------------------
   */
  typedef enum OPNMODE { RDMODE = 1, WRMODE = 0       }  opn_mode_t;
 
  /**-------------------------------------------------------------------------------------------------
   */
  void select  ( )
  { 
    digitalWrite( SPISTE , 0 ); 
  }
  /**-------------------------------------------------------------------------------------------------
   */
  void deselect( )
  { 
    digitalWrite( SPISTE , 1 ); 
  }

  /**-------------------------------------------------------------------------------------------------
   */
  void afe_open( opn_mode_t mode )
  { 
    afe_write( 0x00 , ( uint32_t ) mode );   
  }
  /**-------------------------------------------------------------------------------------------------
   */ 
  void afe_close( ) 
  { 
    deselect( ); 
  }

  /**-------------------------------------------------------------------------------------------------
   */
  void afe_write( uint8_t adr , uint32_t data )
  {
    select();    
    SPI.transfer ( adr ); // send address to device
    SPI.transfer ((data >> 16) & 0xFF); // write top 8 bits
    SPI.transfer ((data >> 8) & 0xFF); // write middle 8 bits
    SPI.transfer (data & 0xFF); // write bottom 8 bits    
    deselect();
  }
  
  void afe_write( uint8_t adr , uint24_t data )
  {
    select();    
    SPI.transfer ( adr ); // send address to device
    SPI.transfer ( data.raw[ 2 ] ); // write top 8 bits
    SPI.transfer ( data.raw[ 1 ] ); // write middle 8 bits
    SPI.transfer ( data.raw[ 0 ] ); // write bottom 8 bits    
    deselect();	  
  }

  /**-------------------------------------------------------------------------------------------------
   */
  uint24_t afe_read( uint8_t adr )
  {
    //char buf[ 4 ];
	uint24_t res;
    select();
    SPI.transfer ( adr ); // send address to device
    //buf[ 3 ] = 0;
    res.raw[ 2 ] = SPI.transfer ( 0 );
    res.raw[ 1 ] = SPI.transfer ( 0 );
    res.raw[ 0 ] = SPI.transfer ( 0 );
    //uint32_t data = *( ( uint32_t* ) buf );
    deselect();
    return res; // return with 24 bits of read data
  }
  /**-------------------------------------------------------------------------------------------------
   */
   void afe_init( );
   
   void afe_set_rdy_cb( void ( *func )() , int irq_n = 0 );
   
  /**-------------------------------------------------------------------------------------------------
   */
  void afe_power( boolean en );
  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void afe_init_alt (void)
{ 
  Serial.println("AFE4490 Initialization Starts"); 
  afe_open( AFE::WRMODE );
  afe_write(CONTROL1, 0x000000);
  afe_write(CONTROL2, 0x000000);  
  afe_write(PRPCOUNT, 0X001F3F);

  afe_write(LED2STC, 0X001770); //timer control
  afe_write(LED2ENDC,0X001F3E); //timer control
  afe_write(LED2LEDSTC,0X001770); //timer control
  afe_write(LED2LEDENDC,0X001F3F); //timer control
  afe_write(ALED2STC, 0X000000); //timer control
  afe_write(ALED2ENDC, 0X0007CE); //timer control
  afe_write(LED2CONVST,0X000002); //timer control
  afe_write(LED2CONVEND, 0X0007CF); //timer control
  afe_write(ALED2CONVST, 0X0007D2); //timer control
  afe_write(ALED2CONVEND,0X000F9F); //timer control

  afe_write(LED1STC, 0X0007D0); //timer control
  afe_write(LED1ENDC, 0X000F9E); //timer control
  afe_write(LED1LEDSTC, 0X0007D0); //timer control
  afe_write(LED1LEDENDC, 0X000F9F); //timer control
  afe_write(ALED1STC, 0X000FA0); //timer control
  afe_write(ALED1ENDC, 0X00176E); //timer control
  afe_write(LED1CONVST, 0X000FA2); //timer control
  afe_write(LED1CONVEND, 0X00176F); //timer control
  afe_write(ALED1CONVST, 0X001772); //timer control
  afe_write(ALED1CONVEND, 0X001F3F); //timer control

  afe_write(ADCRSTCNT0, 0X000000); //timer control
  afe_write(ADCRSTENDCT0,0X000000); //timer control
  afe_write(ADCRSTCNT1, 0X0007D0); //timer control
  afe_write(ADCRSTENDCT1, 0X0007D0); //timer control
  afe_write(ADCRSTCNT2, 0X000FA0); //timer control
  afe_write(ADCRSTENDCT2, 0X000FA0); //timer control
  afe_write(ADCRSTCNT3, 0X001770); //timer control
  afe_write(ADCRSTENDCT3, 0X001770);

//    afe_write(TIAGAIN,0x000000);	// CF = 5pF, RF = 500kR
    afe_write(TIAGAIN,0x000005);	// CF = 5pF, RF = 10kR
    afe_write(TIA_AMB_GAIN,0x000005);	// CF = 5pF, RF = 10kR
    afe_write(LEDCNTRL    ,0x004020);	// 
    afe_write(CONTROL2,0x000000);	// LED_RANGE=100mA, LED=50mA 
    afe_write(CONTROL1,0x000100);	// Timers ON, average 8 samples 


//  delay(1000);
//  Serial.println("AFE4490 Initialization Done"); 
}


};




#endif /* AFE4490_H_ */
