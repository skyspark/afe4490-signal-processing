/**
 * @file crc8.h
 * @brief ���������� ������� CRC8
 */

#ifndef CRC8_H_
#define CRC8_H_

#ifdef __cplusplus
extern "C" {
#endif

#define CRC8_INIT	0xFF

/**-----------------------------------------------------------------------------------
 * ��������� CRC
 * @param crc - ������� ��������
 * @param data - ����� ������
 * @return - ���������� ��������
 */
unsigned char crc8_update( unsigned char crc , unsigned char data );

/**-----------------------------------------------------------------------------------
 * ������� ������� CRC8
 * @param crc - ������� ��������
 * @param len - ����� ������
 * @return - crc �����
 */
unsigned char crc8_blk_calc( unsigned char crc , unsigned char *pcBlock, unsigned int len );

#ifdef __cplusplus
}
#endif

#endif /* CRC8_H_ */
