/*
 * crc16.c
 *
 *  Created on: 07.02.2013
 *      Author: Spark
 */

#include "stdint.h"

/**
 * ��������� CRC �� �������� 0xA001
 * @param crc - ���������� ������� CRC
 * @param a - ������� ����
 * @return ����������� �������� CRC
 */
uint16_t crc16_update_poly( uint16_t crc , uint8_t a )
{
    crc ^= a;
    uint_fast8_t i;
    for ( i = 0 ; i < 8 ; ++i )
    {
        if ( crc & 1 )
            crc = ( crc >> 1 ) ^ 0xA001;
        else
            crc = ( crc >> 1 );
    }
    return crc;
}

/**
 * ��������� CRC �����
 */
uint16_t crc16_update_blk( uint16_t crc , uint8_t* a , uint32_t len )
{
    uint8_t* ptr = a;
    while( len-- )
        crc = crc16_update_poly( crc , *ptr++ );
    return crc;
}
