/**
 * @file crc8.c
 * @brief ���������� ������� CRC8
 */

#include "crc8.h"
/**
  Name  : CRC-8
  Poly  : 0x31    x^8 + x^5 + x^4 + 1
  Init  : 0xFF
  Revert: false
  XorOut: 0x00
  Check : 0xF7 ("123456789")
  MaxLen: 15 ����(127 ���) - �����������
    ���������, �������, ������� � ���� �������� ������
*/

/**-----------------------------------------------------------------------------------
 * ��������� CRC
 * @param crc - ������� ��������
 * @param data - ����� ������
 * @return - ���������� ��������
 */
unsigned char crc8_update( unsigned char crc , unsigned char data )
{
    unsigned int i;
    crc ^= data;

    for( i = 0; i < 8; i++ )
        crc = crc & 0x80 ? ( crc << 1 ) ^ 0x31 : crc << 1;
    return crc;
}

/**-----------------------------------------------------------------------------------
 * ������� ������� CRC8
 * @param crc - ������� ��������
 * @param pcBlock - ��������� �� �����
 * @param len - ����� ������
 * @return - crc �����
 */
unsigned char crc8_blk_calc( unsigned char crc , unsigned char *pcBlock, unsigned int len )
{
    while ( len-- ) crc = crc8_update( crc  , *pcBlock++ );
    return crc;
}



