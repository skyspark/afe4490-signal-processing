/*
 * crc32.h
 *
 *  Created on: 13.02.2013
 *      Author: Spark
 */

#ifndef CRC32_H_
#define CRC32_H_

#define CRC32_INIT 	0xFFFFFFFF

#ifdef __cplusplus
EXTERN_C
{
#endif
/**
 * ��������� CRC �� 0x04C11DB7
 * @param crc - ���������� ������� CRC
 * @param a - ������� ����
 * @return ����������� �������� CRC
 */
uint32_t crc32_update_poly( uint32_t crc , uint8_t a );

/**
 * ��������� CRC �����
 * @param crc - ���������� ������� CRC
 * @param a - ������� ����
 * @param len ����� �����
 * @return ����������� �������� CRC
 */
uint32_t crc32_update_blk( uint32_t crc , uint8_t* a , uint32_t len );
#ifdef __cplusplus
}
#endif
#endif /* CRC32_H_ */
