#ifndef  CBILITE_H
#define  CBILITE_H

#include <stdint.h>
#include <Arduino.h>
#include "crc16.h"



#define SERIAL_BR		512000

/** ------------------------------------------------------------------------------
 * Протокол BLite
 */
class CBLite
{
  public:
  /** -----------------------------------------------------------------------
   * Инициализация 
   */
  void begin( unsigned long br = SERIAL_BR )
  { 
     Serial.begin( br ); 
  }
  /** -----------------------------------------------------------------------
   * Чтение с ожиданием
   * @param buf - буфер
   * @param count;
   */
  static int read( char* buf , unsigned int count , unsigned int to = 25 );
  /** -----------------------------------------------------------------------
   * Чтение пакета
   * @param b cmd - значение комаг   * @param buf 	- буфер данных
   * @param count 	- длина буфера данных
   * @param maxcount - максималоьная 
   */
  int readpkt( char* cmd ,  char* pkt , unsigned int maxcount , uint8_t to = 25 );
  /** -----------------------------------------------------------------------
   * Запись пакета
   * @param cmd 	- команда
   * @param buf 	- буфер данных
   * @param count 	- длина буфера данных
   */
  int writepkt( char cmd , unsigned char* buf , unsigned int count );
};  

#endif