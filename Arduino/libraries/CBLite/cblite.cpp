/** ------------------------------------------------------------------------------
 *
 * Класс протокола BLite для Arduino
 *                                                                                  
 */

 
 #include "cblite.h"
 
  /** ------------------------------------------------------------------------------
   * Протокол BLite
   */
  /** ------------------------------------------------------------------------------
   * Чтение с ожиданием
   */
  int CBLite :: read( char* buf , unsigned int count , unsigned int to )
  {
      int res = 0, to_cnt = to;
      while( count > 0  )
      {
        if( to_cnt <= 0 ) break;
        if( Serial.available( ) <= 0 ) { delay( 1 ); to_cnt--; continue; }
        
        buf[ res++ ] = Serial.read( );
        count--; 
        to_cnt = to;
      };
      return res;
  }
  
   /** -----------------------------------------------------------------------
    * Чтение пакета
    * @param b cmd - значение комаг   * @param buf 	- буфер данных
    * @param count 	- длина буфера данных
    * @param maxcount - максималоьная 
    */
  int CBLite :: readpkt( char* cmd ,  char* pkt , unsigned int maxcount , uint8_t to )
  {
    unsigned int res = 0, len = 0;
    unsigned int crc = CRC16_INIT;
    char buf[ 10 ];
    do
    {
      if ( read( buf , 2 , to )!=2 ) break; 
      if ( buf[ 0 ]!= 'L' || buf[ 1 ]!= 'P' ) { break; };
      
      if ( read( buf , 2 , to ) != 2 ){ break; }
      res = *( ( unsigned int* ) buf ); 
      crc = crc16_update_blk( crc, ( uint8_t* )buf , 2 ); 
      
      if ( !read( cmd , 1 , to ) ) break;
      crc = crc16_update_blk( crc, ( uint8_t* )cmd , 1 ); 
      
      if( res >= maxcount )        break;
      if( read( pkt , res - 1 , to ) != res - 1 ) break;
      crc = crc16_update_blk( crc, ( uint8_t*  )pkt , res - 1 );
      
      if ( read( buf , 2 , to ) != 2 ) break;
      if( crc != *( ( unsigned int* ) buf ) ) break;
      len = res;
    }while(0);
    return len;
  }
   /** -----------------------------------------------------------------------
   * Запись пакета
   * @param cmd 	- команда
   * @param buf 	- буфер данных
   * @param count 	- длина буфера данных
   */
  int CBLite :: writepkt( char cmd , unsigned char* buf , unsigned int count )
  {
    unsigned int crc = CRC16_INIT;
    Serial.write( ( uint8_t* )"LP" , 2 );
       
    uint16_t pdu_len = count + 1; // ( data * n_byte ) + CMD 
    Serial.write( ( uint8_t* )&pdu_len , 2 );
    crc = crc16_update_blk( crc, ( uint8_t* )( &pdu_len ) , 2 );

    Serial.write( ( uint8_t* )&cmd , 1 );
    crc = crc16_update_blk( crc, ( uint8_t* )&cmd , 1 );
    Serial.write( ( uint8_t* )buf , count );
    crc = crc16_update_blk( crc, ( uint8_t* )buf , count );
    Serial.write( ( uint8_t* )&crc , 2 );
    
    return count;
  }